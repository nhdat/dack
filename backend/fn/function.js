var express = require('express');
var jwt = require('jsonwebtoken');
var multer = require('multer');
var random = require('random-integer');

var secret_key = "dack";

// verify token
exports.fnCheckToken = function(req, res, next){
	var token = req.headers['x-access-token'];
	if(token){
		jwt.verify(token, secret_key, (err, payload) => {
			if(err){
				res.statusCode = 401;
				res.json({
					msg: 'verify fail',
					error: err
				});
			}else{
				req.payload = payload;
				next();
			}
		});
	}else{
		res.statusCode = 402;
		res.json({
			msg: 'no token found'
		});
	}
}


// Path of upload file
var storage = multer.diskStorage({
    destination: function(req, file, cb) {
        cb(null, './public/imgs')
    },
    filename: function(req, file, cb) {
    	var newName = (file.originalname).split('.')[0] + "_" + random(1, 50) + random(50, 99) + "." + (file.mimetype).split('/')[1];
        cb(null, newName);
    }
});

exports.upload = multer({
    storage: storage
});

exports.convertNumber = function (number){
    return number.replace(/,/g, '');
}

exports.formatDateTime = function (val){
    var date = val.split(' ')[0];

    var year = date.split('/')[2];
    var month = date.split('/')[1];
    var day = date.split('/')[0];

    var time = val.split(' ')[1];

    var hours = time.split(':')[0];
    var minutes = time.split(':')[1];

    var str = year + "-" + month + "-" + day + " " + hours + ":" + minutes;
    return str;
}

exports.formatDateTimeUser = function(date){
	var day = date.getDate() < 10 ? "0" + date.getDate() : date.getDate();
    var month = date.getMonth() < 10 ? "0" + date.getMonth() : date.getMonth();
    var year = date.getFullYear();

    var hours = date.getHours() < 10 ? "0" + date.getHours() : date.getHours();
    var minutes = date.getMinutes() < 10 ? "0" + date.getMinutes() : date.getMinutes();
    var second = date.getSeconds() < 10 ? "0" + date.getSeconds() : date.getSeconds();

    var str = year + "-" + month + "-" + day + " " + hours + ":" + minutes + ":" + second;
    return str;
}

exports.formatDate = function(date){
    var day = date.getDate() < 10 ? "0" + date.getDate() : date.getDate();
    var month = date.getMonth() < 10 ? "0" + date.getMonth() : date.getMonth();
    var year = date.getFullYear();

    var str = day + "-" + month + "-" + year;
    return str;
}