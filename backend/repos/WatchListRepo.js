var watchlist = require('../schemas').watchlist;

// Load All
exports.loadAll = function () {
    return watchlist.findAll({
        raw: true
    });
}

// Load By User ID
exports.loadByUserId = function (poco) {
    return watchlist.findAll(
    {
        where: {
            WlUserID: poco.WlUserID
        }
    });
    //return watchlist.findById(poco.WlUserID);
}

// Insert
exports.insert = function (poco) {
    return watchlist.create({ 
        WlUserID: poco.WlUserID,
        WlPrID: poco.WlPrID
    });
}

// Update
exports.update = function (poco) {
    return "not complete";
}

// Delete
exports.delete = function(poco){
    return watchlist.destroy({
        where: {
            WlUserID: poco.WlUserID,
            WlPrID: poco.WlPrID
        }
    });
}