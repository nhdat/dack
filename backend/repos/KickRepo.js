var kick = require('../schemas').kick;

// Load All
exports.loadAll = function () {
    return kick.findAll({
        raw: true
    });
}

// Load By User ID
exports.loadByUserId = function (poco) {
    return kick.findAll(
    {
        where: {
            KickUserID: poco.KickUserID
        }
    });
    //return watchlist.findById(poco.WlUserID);
}

// Load By User ID
exports.loadByUserIDaPrID = function (poco) {
    return kick.findAll(
    {
        where: {
            KickUserID: poco.KickUserID,
            KickPrID: poco.KickPrID
        }
    });
    //return watchlist.findById(poco.WlUserID);
}

// Insert
exports.insert = function (poco) {
    return kick.create({ 
        KickUserID: poco.KickUserID,
        KickPrID: poco.KickPrID
    });
}

// Update
exports.update = function (poco) {
    return "not complete";
}

// Delete
exports.delete = function(poco){
    return kick.destroy({
        where: {
            KickUserID: poco.KickUserID,
            KickPrID: poco.KickPrID
        }
    });
}