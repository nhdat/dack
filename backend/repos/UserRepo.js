var user = require('../schemas').user;
const Op = require('../schemas').Op;
var constant = require('../fn/constant.js');
var formatDateTimeUser = require('../fn/function.js').formatDateTimeUser;
const limit = constant.limit;

// Load All
exports.loadAll = function() {
    return user.findAll({
        raw: true
    });
}

// Load By ID
exports.loadById = function(poco) {
    return user.findById(poco.UserID);
}

// Load By Email --- return email
exports.loadByEmail = function(poco) {
    return user.findAll({
        where: {
            UserEmail: poco.UserEmail
        }
    });
}

// Load By Time Permission
exports.loadByTimePermission = function(poco) {
    var page = poco.page;
    return user.findAll({
        where: {
            UserTimePermission: {
                [Op.not]: null
            },
            UserType: 'buy'
        },
        offset: (page * limit) - limit,
        limit: limit + 1,
        order: [
            ['UserTimePermission', 'ASC']
        ]
    });
}

// Load By Page
exports.loadByPage = function(poco) {
    var page = poco.page;
    return user.findAll({
        where: {
            UserLevel : 1
        },
        offset: (page * limit) - limit,
        limit: limit + 1
    });
}

// Load By Email and Pwd --- return email + pwd
exports.loadByEmailaPwd = function(poco) {
    return user.findAll({
        where: {
            UserEmail: poco.UserEmail,
            UserPwd: poco.UserPwd
        }
    });
}

// Insert
exports.insert = function(poco) {
    return user.create({
        UserName: poco.UserName,
        UserEmail: poco.UserEmail,
        UserPwd: poco.UserPwd,
        UserAdd: poco.UserAdd
    });
}

exports.requestseller = function(poco){
    var date = new Date();
    date.setMonth(date.getMonth() + 1);

    var str = formatDateTimeUser(date);

    return user.update({
        UserTimePermission: str
    }, {
        where: {
            UserID: poco.UserID
        }
    });
}

// Update Seller
exports.updateSeller = function(poco) {
    var date = new Date();
    date.setDate(date.getDate() + 7);

    var str = formatDateTimeUser(date);

    return user.update({
        UserType: 'sell',
        UserTimePermission: null,
        UserTimeEndSeller: str
    }, {
        where: {
            UserID: poco.UserID
        }
    });
}

// Update Buyer
exports.updateBuyer = function(poco) {
    return user.update({
        UserType: 'buy',
        UserTimePermission: null,
        UserTimeEndSeller: null
    }, {
        where: {
            UserID: poco.UserID
        }
    });
}

// Update all
exports.update = function(poco) {
    if(poco.UserEmail == null){
        return user.update({
            UserName: poco.UserName,
            UserPwd: poco.UserPwd,
            UserAdd: poco.UserAdd
        }, {
            where: {
                UserID: poco.UserID
            }
        });
    }

    return user.update({
        UserName: poco.UserName,
        UserEmail: poco.UserEmail,
        UserPwd: poco.UserPwd,
        UserAdd: poco.UserAdd
    }, {
        where: {
            UserID: poco.UserID
        }
    });
}

// Resert Password
exports.updatePwd = function(poco) {
    return user.update({
        UserPwd: poco.UserPwd
    }, {
        where: {
            UserID: poco.UserID
        }
    });
}

// Resert Password
exports.checkPwd = function(poco) {
    return user.findAll({
        where: {
            UserID: poco.UserID,
            UserPwd: poco.UserPwd
        }
    });
}

// Delete
exports.delete = function(poco) {
    return user.destroy({
        where: {
            UserID: poco.UserID
        }
    });
}