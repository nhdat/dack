var cat = require('../schemas').cat;
var constant = require('../fn/constant.js');
const limit = constant.limit;

// Load All
exports.loadAll = function () {
    return cat.findAll({
        raw: true
    });
}

exports.loadByPage = function(poco){
    var page = poco.page;

    return cat.findAll({
        offset: (page * limit) - limit, 
        limit: limit + 1
    });
}

// Load By ID
exports.loadById = function (poco) {
    return cat.findById(poco.CatID);
}

// Insert
exports.insert = function (poco) {
    return cat.create({ CatName: poco.CatName });
}

// Update
exports.update = function (poco) {
    return cat.update({
        CatName: poco.CatName
    },{
        where: {
            CatID: poco.CatID
        }
    });
}

// Delete
exports.delete = function(poco){
    return cat.destroy({
        where: {
            CatID: poco.CatID
        }
    });
}