var product = require('../schemas').product;
var bid = require('../schemas.js').bid;
var img = require('../schemas.js').img;
var bidRepo = require('../repos/BidRepo.js');
var watchlist = require('../schemas.js').watchlist;
var bidRepo = require('./BidRepo.js');
var constant = require('../fn/constant.js');
const limit = constant.limit;
const Op = require('../schemas').Op;
var constant = require('../fn/constant.js');
var formatDateTimeUser = require('../fn/function.js').formatDateTimeUser;
var formatDate = require('../fn/function.js').formatDate;
var sequelize = require('../schemas.js').sequelize;

// Load All
exports.loadAll = function () {
    return product.findAll({
        raw: true
    });
}

// Load By ID
exports.loadById = function (poco) {
    return product.findById(poco.PrID);
}

// Load By ID and return UserName Seller
exports.loadByIdandSeller = function (poco) {
    var PrID = poco.PrID;
    var sql = `select p.*, u.UserName as PrUserNameSeller
                from product as p, user as u
                where p.PrUserID = u.UserID
                    and p.PrID = ${PrID}
                GROUP BY p.PrID`;
    return sequelize.query(sql, {
        type: sequelize.QueryTypes.SELECT
    });
}

// Load bid list
exports.loadBidList = function (poco) {
    var PrID = poco.PrID;
    var sql = `select b.*, u.UserName as BidUserName
                from product as p, bid as b, user as u
                where p.PrID = b.BidPrID
                    and u.UserID = b.BidUserID
                    and p.PrID = ${PrID}
                order by b.BidID DESC`;
    return sequelize.query(sql, {
        type: sequelize.QueryTypes.SELECT
    });
}

// Load By ID and return img of product
product.belongsTo(img, {
    foreignKey: 'PrID', 
    targetKey: 'ImgPrID'
});
exports.loadByIDaImg = function(poco){
    return product.findAll({
        where: {
            PrID: poco.PrID
        },
        include: [{
            model: img,
            required: true
        }],
    });
}

// Load By Page
exports.loadByPage = function(poco){
    var page = poco.page;

    return product.findAll({
        where: {
            PrUserID: poco.UserID
        },
        offset: (page * limit) - limit, 
        limit: limit + 1,
        order: [
            ['PrID', 'DESC']
        ]
    });
}

// Load By Page Not End
exports.loadByPageNotEnd = function(poco){
    var date = new Date();
    date.setMonth(date.getMonth() + 1);

    var dateCurrent = formatDateTimeUser(date);

    var page = poco.page;

    return product.findAll({
        where: {
            PrUserID: poco.UserID,
            PrTimeEnd: {
                [Op.gt] : dateCurrent
            }
        },
        offset: (page * limit) - limit, 
        limit: limit + 1,
        order: [
            ['PrID', 'DESC']
        ]
    });
}


// Load By Page Sold
product.belongsTo(bid, {
    foreignKey: 'PrID', 
    targetKey: 'BidPrID'
});
exports.loadByPageSold = function(poco){
    var date = new Date();
    date.setMonth(date.getMonth() + 1);

    var dateCurrent = formatDateTimeUser(date);
    var page = poco.page;

    return product.findAll({
        where: {
            PrUserID: poco.UserID,
            PrTimeEnd: {
                [Op.lt] : dateCurrent
            }
        },
        offset: (page * limit) - limit, 
        limit: limit + 1,
        order: [
            ['PrID', 'DESC']
        ],
        include: [{
            model: bid,
            required: true
        }],
        raw: true,
        group: ['PrID']
    });
}

// Load By Page Watch List
product.belongsTo(watchlist, {
    foreignKey: 'PrID', 
    targetKey: 'WlPrID'
});
exports.loadByPageWatchList = function(poco){
    var page = poco.page;

    return product.findAll({
        offset: (page * limit) - limit, 
        limit: limit + 1,
        order: [
            ['PrID', 'DESC']
        ],
        include: [{
            model: watchlist,
            required: true,
            where: {
                WlUserID: poco.UserID
            },
        }],
        raw: true
    });
}

// Load By Page Bidded and win
exports.loadByPageBiddedandSuccess = function(poco){
    var date = new Date();
    date.setMonth(date.getMonth() + 1);

    var dateCurrent = formatDateTimeUser(date);
    var page = poco.page;

    var offset = (page * limit) - limit;
    var limitReal = limit + 1;

    var sql = `select p.*
                from product as p, bid as b
                where p.PrID = b.BidPrID
                    and b.BidUserID = ${poco.UserID}
                    and p.PrTimeEnd < '${dateCurrent}'
                    and b.BidTotal >= all(select b2.BidTotal
                                            from bid as b2
                                            where b2.BidPrID = b.BidPrID)
                group by p.PrID
                limit ${limitReal} offset ${offset}`;
    return sequelize.query(sql, {
        type: sequelize.QueryTypes.SELECT
    });
}

// Load By Page Bidding
exports.loadByPageBidding = function(poco){
    var date = new Date();
    date.setMonth(date.getMonth() + 1);

    var dateCurrent = formatDateTimeUser(date);
    var page = poco.page;

    return product.findAll({
        where: {
            PrTimeEnd: {
                [Op.gt] : dateCurrent // >
            }
        },
        offset: (page * limit) - limit, 
        limit: limit + 1,
        order: [
            ['PrID', 'DESC']
        ],
        include: [{
            model: bid,
            required: true,
            where: {
                BidUserID: poco.UserID
            }
        }],
        raw: true,
        group: ['PrID']
    });
}

// Insert
exports.insert = function (poco) {
    return product.create({
    	PrName : poco.PrName,
        PrCatID: poco.PrCatID,
        PrUserID: poco.PrUserID,
        PrAutoReup: poco.PrAutoReup,
        PrPriceOffer: poco.PrPriceOffer,
        PrPriceStep: poco.PrPriceStep,
        PrPriceBuyNow: poco.PrPriceBuyNow,
        PrTimeStart: poco.PrTimeStart,
        PrTimeEnd: poco.PrTimeEnd,
        PrContent: poco.PrContent
    });
}

// Update
exports.updateContent = function (poco) {
    var date = new Date();
    date.setMonth(date.getMonth() + 1);
    var dateCurrent = formatDate(date);

    var PrContent = poco.PrContentOld +
                    "<b>Update (" + dateCurrent + ") </b>" +
                    poco.PrContentNew;

    return product.update({
        PrContent: PrContent
    },{
        where: {
            PrID: poco.PrID
        }
    });
}

// Delete
exports.delete = function(poco){
    return product.destroy({
        where: {
            PrID: poco.PrID
        }
    });
}

// Top 5 sản phẩm có nhiều lượt ra giá nhất
exports.top5NhieuLuotRaGia = function(){
    var date = new Date();
    date.setMonth(date.getMonth() + 1);

    var dateCurrent = formatDateTimeUser(date);

    var sql = `select p.*, i.*, count(b.BidID) as CountBid
                from product as p
                LEFT JOIN bid as b on b.BidPrID = p.PrID
                LEFT JOIN img as i on i.ImgPrID = p.PrID
                where p.PrTimeEnd > '${dateCurrent}'
                    and p.PrDelayStatus = 'on'
                group by p.PrID
                order by countBid DESC
                limit 5 offset 0`;

    return sequelize.query(sql, {
        type: sequelize.QueryTypes.SELECT
    });
}

// Top 5 sản phẩm có giá cao nhất
exports.top5GiaCaoNhat = function(){
    var date = new Date();
    date.setMonth(date.getMonth() + 1);

    var dateCurrent = formatDateTimeUser(date);

    var sql = `select p.*, i.*, max(b.BidTotal) as max
                from product as p, bid as b, img as i
                where p.PrID = b.BidPrID
                    and p.PrID = i.ImgPrID
                    and p.PrTimeEnd > '${dateCurrent}'
                    and p.PrDelayStatus = 'on'
                group by b.BidPrID
                order by max DESC
                limit 5 offset 0`;

    return sequelize.query(sql, {
        type: sequelize.QueryTypes.SELECT
    });
}

// Top 5 sản phẩm gần kết thúc
exports.top5GanKetThuc = function(){
    var date = new Date();
    date.setMonth(date.getMonth() + 1);

    var dateCurrent = formatDateTimeUser(date);

    var sql = `select *
                from product as p, img as i
                where p.PrTimeEnd > '${dateCurrent}'
                    and p.PrDelayStatus = 'on'
                    and p.PrID = i.ImgPrID
                group by p.PrID
                order by p.PrTimeEnd ASC
                limit 5 offset 0`;

    return sequelize.query(sql, {
        type: sequelize.QueryTypes.SELECT
    });
}

// Find product without categories
exports.findProductWithoutCat = function(poco){
    var date = new Date();
    date.setMonth(date.getMonth() + 1);

    var dateCurrent = formatDateTimeUser(date);

    var query = poco.query;
    var page = poco.page;

    var offset = (limit * page) - limit;
    var limitReal = limit + 1;
    var sql = `select *
                from product as p, img as i
                where p.PrName like '%${query}%'
                    and p.PrID = i.ImgPrID
                    and p.PrTimeEnd > '${dateCurrent}'
                    and p.PrDelayStatus = 'on'
                group by p.PrID
                limit ${limitReal} offset ${offset}`;

    return sequelize.query(sql, {
        type: sequelize.QueryTypes.SELECT
    });
}

// Find product with categories
exports.findProductWithCat = function(poco){
    var date = new Date();
    date.setMonth(date.getMonth() + 1);

    var dateCurrent = formatDateTimeUser(date);
    
    var query = poco.query;
    var cat = poco.cat;
    var page = poco.page;

    var offset = (limit * page) - limit;
    var limitReal = limit + 1;
    var sql = `select *
                from product as p, img as i
                where p.PrName like '%${query}%'
                    and p.PrCatID = ${cat}
                    and p.PrID = i.ImgPrID
                    and p.PrTimeEnd > '${dateCurrent}'
                    and p.PrDelayStatus = 'on'
                group by p.PrID
                limit ${limitReal} offset ${offset}`;

    return sequelize.query(sql, {
        type: sequelize.QueryTypes.SELECT
    });
}

exports.findProductOff = function(poco){
    var sql = `select *
                from product as p
                where p.PrDelayStatus = 'off'
                    and p.PrTimeStart > '${poco.dateCurrent}'`;
    return sequelize.query(sql, {
        type: sequelize.QueryTypes.SELECT
    });
}

exports.updateProductOn = function(poco){
    return product.update({
        PrDelayStatus: 'on'
    },{
        where: {
            PrID: poco.PrID
        }
    });
}

exports.findProductReup = function(poco){
    var sql = `select *
                from product as p
                where TIMEDIFF(p.PrTimeEnd, '${poco.dateCurrent}') <= '00:05:00'
                    and TIMEDIFF(p.PrTimeEnd, '${poco.dateCurrent}') >= '00:00:00'
                    and p.PrAutoReup = 1`;
    return sequelize.query(sql, {
        type: sequelize.QueryTypes.SELECT
    });
}

exports.findUserBidReup = function(poco){
    var sql = `select *
                from bid as b
                where b.BidPrID = ${poco.BidPrID}
                    and TIMEDIFF('${poco.PrTimeEnd}', b.BidTimeBid) <= '00:05:00'
                    and TIMEDIFF('${poco.PrTimeEnd}', b.BidTimeBid) >= '00:00:00'`;
    return sequelize.query(sql, {
        type: sequelize.QueryTypes.SELECT
    });
}

exports.updateProductReup = function(poco){
    var PrTimeEnd = poco.PrTimeEnd;

    var sql = `update product as p
                set p.PrTimeEnd = p.PrTimeEnd + INTERVAL 10 MINUTE
                where p.PrID = ${poco.PrID}`;
    return sequelize.query(sql);
}

exports.findProductTimeOut = function(poco){
    var sql = `select *
                from product as p
                where TIMEDIFF(p.PrTimeEnd, '${poco.dateCurrent}') <= '00:01:00'
                    and TIMEDIFF(p.PrTimeEnd, '${poco.dateCurrent}') >= '00:00:00'`;
    return sequelize.query(sql, {
        type: sequelize.QueryTypes.SELECT
    });
}