var bid = require('../schemas').bid;
var sequelize = require('../schemas.js').sequelize;

// Load All
exports.loadAll = function () {
    return bid.findAll({
        raw: true
    });
}

// Load By User ID
exports.loadByUserId = function (poco) {
    return bid.findAll(
    {
        where: {
            BidUserID: poco.BidUserID
        }
    });
}

// Load By Bid D
exports.loadByBidId = function (poco) {
    var sql = `select *
                from bid as b, user as u
                where b.BidID = ${poco.BidID}
                    and b.BidUserID = u.UserID
                order by b.BidID DESC
                limit 1 offset 0`;

    return sequelize.query(sql, {
        type: sequelize.QueryTypes.SELECT
    });
}

// Load By Product ID
exports.loadByProductId = function (poco) {
    return bid.findAll(
    {
        where: {
            BidPrID: poco.BidPrID
        },
        order: [
            ['BidID', 'DESC']
        ],
        raw: true
    });
}

// Load By Product ID and return max Bid
exports.loadMaxBidByProductID = function (poco) {
    var sql = `select *
                from bid as b, user as u
                where b.BidPrID = ${poco.BidPrID}
                    and b.BidUserID = u.UserID
                order by b.BidID DESC
                limit 1 offset 0`;

    return sequelize.query(sql, {
        type: sequelize.QueryTypes.SELECT
    });
}


// Insert
exports.insert = function (poco) {
    return bid.create({ 
        BidUserID: poco.BidUserID,
        BidPrID: poco.BidPrID,
        BidTimeBid: poco.BidTimeBid,
        BidTotal: poco.BidTotal,
        BidPriceMax:poco.BidPriceMax
    });
}

// Update
exports.update = function (poco) {
    return "not complete";
}

// Delete by BidPrID
exports.deleteByBidID = function(poco){
    return bid.destroy({
        where: {
            BidID: poco.BidID
        }
    });
}

// Delete
exports.delete = function(poco){
    return bid.destroy({
        where: {
            BidUserID: poco.BidUserID,
            BidPrID: poco.BidPrID
        }
    });
}