var img = require('../schemas').img;

// Load All
exports.loadAll = function () {
    return img.findAll({
        raw: true
    });
}

// Load By ID
exports.loadByPrId = function (poco) {
    return img.findAll({
        where: {
            ImgPrID: poco.ImgPrID
        },
        raw: true
    });
    //return img.findById(poco.PrID);
}

// Insert
exports.insert = function (poco) {
    return img.create({ 
        ImgName: poco.ImgName,
        ImgPrID: poco.ImgPrID
    });
}

// Update
exports.update = function (poco) {
    return img.update({
        ImgName: poco.ImgName,
        ImgPrID: poco.ImgPrID
    },{
        where: {
            ImgID: poco.ImgID
        }
    });
}

// Delete
exports.delete = function(poco){
    return img.destroy({
        where: {
            ImgID: poco.ImgID
        }
    });
}