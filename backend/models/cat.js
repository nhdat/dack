/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('cat', {
    CatID: {
      type: DataTypes.INTEGER(10).UNSIGNED,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    CatName: {
      type: DataTypes.STRING(255),
      allowNull: true
    }
  }, {
    tableName: 'cat',
    createdAt: false,
    updatedAt: false
  });
};
