/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('watchlist', {
    WlUserID: {
      type: DataTypes.INTEGER(10).UNSIGNED,
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'user',
        key: 'UserID'
      }
    },
    WlPrID: {
      type: DataTypes.INTEGER(10).UNSIGNED,
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'product',
        key: 'PrID'
      }
    }
  }, {
    tableName: 'watchlist',
    createdAt: false,
    updatedAt: false
  });
};
