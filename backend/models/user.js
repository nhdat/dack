/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('user', {
    UserID: {
      type: DataTypes.INTEGER(10).UNSIGNED,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    UserName: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    UserEmail: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    UserPwd: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    UserAdd: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    UserLevel: {
      type: DataTypes.INTEGER(2).UNSIGNED,
      allowNull: true,
      defaultValue: '1'
    },
    UserType: {
      type: DataTypes.STRING(255),
      allowNull: true,
      defaultValue: 'buy'
    },
    UserTimePermission: {
      type: DataTypes.DATE,
      allowNull: true
    },
    UserTimeEndSeller: {
      type: DataTypes.DATE,
      allowNull: true
    }
  }, {
    tableName: 'user',
    createdAt: false,
    updatedAt: false
  });
};
