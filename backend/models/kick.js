/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('kick', {
    KickPrID: {
      type: DataTypes.INTEGER(10).UNSIGNED,
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'product',
        key: 'PrID'
      }
    },
    KickUserID: {
      type: DataTypes.INTEGER(10).UNSIGNED,
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'user',
        key: 'UserID'
      }
    }
  }, {
    tableName: 'kick',
    createdAt: false,
    updatedAt: false
  });
};
