/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('img', {
    ImgID: {
      type: DataTypes.INTEGER(10).UNSIGNED,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    ImgName: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    ImgPrID: {
      type: DataTypes.INTEGER(10).UNSIGNED,
      allowNull: true,
      references: {
        model: 'product',
        key: 'PrID'
      }
    }
  }, {
    tableName: 'img',
    createdAt: false,
    updatedAt: false
  });
};
