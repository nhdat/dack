/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('product', {
    PrID: {
      type: DataTypes.INTEGER(10).UNSIGNED,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    PrName: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    PrContent: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    PrCatID: {
      type: DataTypes.INTEGER(10).UNSIGNED,
      allowNull: true,
      references: {
        model: 'cat',
        key: 'CatID'
      }
    },
    PrUserID: {
      type: DataTypes.INTEGER(10).UNSIGNED,
      allowNull: true,
      references: {
        model: 'user',
        key: 'UserID'
      }
    },
    PrAutoReup: {
      type: DataTypes.INTEGER(2),
      allowNull: true
    },
    PrPriceOffer: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    PrPriceStep: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    PrPriceBuyNow: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    PrTimeStart: {
      type: DataTypes.DATE,
      allowNull: true
    },
    PrTimeEnd: {
      type: DataTypes.DATE,
      allowNull: true
    },
    PrDelayStatus: {
      type: DataTypes.STRING(255),
      allowNull: true,
      defaultValue: 'off'
    }
  }, {
    tableName: 'product',
    createdAt: false,
    updatedAt: false
  });
};
