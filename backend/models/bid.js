/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('bid', {
    BidID: {
      type: DataTypes.INTEGER(10).UNSIGNED,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    BidPrID: {
      type: DataTypes.INTEGER(10).UNSIGNED,
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'product',
        key: 'PrID'
      }
    },
    BidUserID: {
      type: DataTypes.INTEGER(10).UNSIGNED,
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'user',
        key: 'UserID'
      }
    },
    BidTimeBid: {
      type: DataTypes.DATE,
      allowNull: true
    },
    BidTotal: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    BidPriceMax: {
      type: DataTypes.FLOAT,
      allowNull: true
    }
  }, {
    tableName: 'bid',
    createdAt: false,
    updatedAt: false
  });
};
