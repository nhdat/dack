var BidRepo = require('../repos/BidRepo.js');
var PrRepo = require('../repos/ProductRepo.js');
var fnCheckToken = require('../fn/function.js').fnCheckToken;
var formatDateTimeUser = require('../fn/function.js').formatDateTimeUser;
var KickRepo = require('../repos/KickRepo.js');
var nodemailer = require('nodemailer');
var UserRepo = require('../repos/UserRepo.js');

var express = require('express');
var router = express.Router();

// Load All
router.get('/', (req, res) => {
    BidRepo.loadAll().then(rows => {
        res.json(rows);
    }).catch(err => {
        res.statusCode = 500;
        res.json(err);
    });
});

// Load By User ID
router.get('/user/:id', (req, res) => {
    if (req.params.id) {
        var id = req.params.id;
        if (isNaN(id)) {
            res.statusCode = 400;
            res.end('ID is not valid.');
            return;
        }

        var poco = {
            BidUserID: id
        }
        BidRepo.loadByUserId(poco).then(rows => {
            res.json(rows);
        }).catch(err => {
            res.statusCode = 500;
            res.json(err);
        });
    } else {
        res.statusCode = 400;
        res.json('Error');
    }
});

// Load By BidID
router.get('/bid/:id', (req, res) => {
    if (req.params.id) {
        var id = req.params.id;
        if (isNaN(id)) {
            res.statusCode = 400;
            res.end('ID is not valid.');
            return;
        }

        var poco = {
            BidID: id
        }
        BidRepo.loadByBidId(poco).then(rows => {
            rows = rows[0];
            res.json(rows);
        }).catch(err => {
            res.statusCode = 500;
            res.json(err);
        });
    } else {
        res.statusCode = 400;
        res.json('Error');
    }
});

// Load By Product ID
router.get('/product/:id', (req, res) => {
    if (req.params.id) {
        var id = req.params.id;
        if (isNaN(id)) {
            res.statusCode = 400;
            res.end('ID is not valid.');
            return;
        }

        var poco = {
            BidPrID: id
        }
        BidRepo.loadByProductId(poco).then(rows => {
            res.json(rows);
        }).catch(err => {
            res.statusCode = 500;
            res.json(err);
        });
    } else {
        res.statusCode = 400;
        res.json('Error');
    }
});

// Insert normal
router.post('/', fnCheckToken, (req, res) => {
    var date = new Date();
    date.setMonth(date.getMonth() + 1);
    var BidTimeBid = formatDateTimeUser(date);

    var BidUserID = req.body.BidUserID;
    var BidPrID = req.body.BidPrID;
    BidTimeBid = BidTimeBid;
    var BidTotal = req.body.BidTotal;
    var BidPriceMax = req.body.BidPriceMax;

    var poco = {
        KickUserID: BidUserID,
        KickPrID: BidPrID
    }

    KickRepo.loadByUserIDaPrID(poco).then(rs => {
        console.log(rs);
        if(rs.length > 0){
            res.statusCode = 400;
            res.json({
                msg: "Kicked"
            });
            return;
        }

        var poco = {
            PrID: BidPrID
        }
        // Load Product
        PrRepo.loadById(poco).then(product => {
            var PrPriceStep = product.PrPriceStep;
            PrPriceStep = parseInt(PrPriceStep);

            var PrPriceOffer = product.PrPriceOffer;
            PrPriceOffer = parseInt(PrPriceOffer);

            // Load list Bid by PrID
            var poco = {
                BidPrID: BidPrID
            }
            BidRepo.loadByProductId(poco).then(bidList => {
                //console.log(bidList);
                if (bidList.length > 0) {
                    // Đã có người đấu giá sản phẩm này.                
                    var bidMax = bidList[0]; // Người đầu tiên trong danh sách là người có đang giữ giá.
                    var bidMaxPriceMax = bidMax.BidPriceMax;

                    var bidMaxTotal = bidMax.BidTotal;
                    bidMaxTotal = parseInt(bidMaxTotal);

                    if (bidMaxPriceMax == null) {
                        // Người giữ giá không sử dụng Auto-Bid => Insert cho người mới luôn.   
                        var poco = {
                            BidUserID: BidUserID,
                            BidPrID: BidPrID,
                            BidTimeBid: BidTimeBid,
                            BidTotal: BidTotal,
                            BidPriceMax: BidPriceMax
                        }

                        BidRepo.insert(poco).then(rs => {
                            sendMailBidUser(poco);
                            res.json(rs);
                        }).catch(err => {
                            res.statusCode = 500;
                            res.json("1");
                        });
                    } else {
                        bidMaxPriceMax = parseInt(bidMaxPriceMax);
                        // Người giữ giá đang sử dụng Auto-Bid.
                        // Người dùng không dùng Auto-Bid (Bid truyền thống)
                        if (BidPriceMax == null) {

                            // Insert cho người dùng
                            var poco = {
                                BidUserID: BidUserID,
                                BidPrID: BidPrID,
                                BidTimeBid: BidTimeBid,
                                BidTotal: BidTotal,
                                BidPriceMax: BidPriceMax
                            }

                            BidRepo.insert(poco).then(rsUser => {
                                // Insert cho người giữ giá                    
                                // So sánh nếu PriceMax của người giữ giá còn có thể insert dc
                                rsUser.BidTotal = parseInt(rsUser.BidTotal);
                                if (bidMaxPriceMax >= (rsUser.BidTotal + PrPriceStep)) {
                                    var poco = {
                                        BidUserID: bidMax.BidUserID,
                                        BidPrID: bidMax.BidPrID,
                                        BidTimeBid: BidTimeBid,
                                        BidTotal: (rsUser.BidTotal + PrPriceStep),
                                        BidPriceMax: bidMax.BidPriceMax
                                    }

                                    // Insert cho người giữ giá
                                    BidRepo.insert(poco).then(rsHolder => {
                                        sendMailBidUser(poco);
                                        res.json(rsHolder);
                                        
                                    }).catch(err => {
                                        res.statusCode = 500;
                                        res.json("2");
                                    });
                                } else if (bidMaxPriceMax == rsUser.BidTotal) {
                                    var poco = {
                                        BidUserID: bidMax.BidUserID,
                                        BidPrID: bidMax.BidPrID,
                                        BidTimeBid: BidTimeBid,
                                        BidTotal: bidMaxPriceMax,
                                        BidPriceMax: bidMax.BidPriceMax
                                    }

                                    // Insert cho người giữ giá
                                    BidRepo.insert(poco).then(rsHolder => {
                                        sendMailBidUser(poco);
                                        res.json(rsHolder);
                                        
                                    }).catch(err => {
                                        res.statusCode = 500;
                                        res.json("2");
                                    });
                                } else {
                                    sendMailBidUser(poco);
                                    res.json(rsUser);
                                }
                            }).catch(err => {
                                res.statusCode = 500;
                                res.json("3");
                            });
                        } else {
                            // Người dùng sử dụng Auto-Bid => So sánh theo trường hợp.
                            // TH1: PriceMax của người giữ giá và người dùng = nhau
                            BidPriceMax = parseInt(BidPriceMax);
                            if (bidMaxPriceMax == BidPriceMax) {
                                // Insert cho người dùng
                                var poco = {
                                    BidUserID: BidUserID,
                                    BidPrID: BidPrID,
                                    BidTimeBid: BidTimeBid,
                                    BidTotal: BidPriceMax,
                                    BidPriceMax: BidPriceMax
                                }

                                BidRepo.insert(poco).then(rsUser => {
                                    // Insert cho người giữ giá
                                    var poco = {
                                        BidUserID: bidMax.BidUserID,
                                        BidPrID: bidMax.BidPrID,
                                        BidTimeBid: BidTimeBid,
                                        BidTotal: bidMax.BidPriceMax,
                                        BidPriceMax: bidMax.BidPriceMax
                                    }


                                    BidRepo.insert(poco).then(rsHolder => {
                                        sendMailBidUser(poco);
                                        res.json(rsHolder);
                                        
                                    }).catch(err => {
                                        res.statusCode = 500;
                                        res.json("4");
                                    });
                                }).catch(err => {
                                    res.statusCode = 500;
                                    res.json("5");
                                });
                            }
                            // TH2: PriceMax của người giữ giá > người dùng
                            if (bidMaxPriceMax > BidPriceMax) {
                                // Insert cho người dùng
                                var poco = {
                                    BidUserID: BidUserID,
                                    BidPrID: BidPrID,
                                    BidTimeBid: BidTimeBid,
                                    BidTotal: BidPriceMax,
                                    BidPriceMax: BidPriceMax
                                }

                                BidRepo.insert(poco).then(rsUser => {
                                    // Insert cho người giữ giá
                                    // Xét xem người giữ giá có đủ tiền để insert
                                    if (bidMax.BidPriceMax >= (rsUser.BidTotal + PrPriceStep)) {
                                        var poco = {
                                            BidUserID: bidMax.BidUserID,
                                            BidPrID: bidMax.BidPrID,
                                            BidTimeBid: BidTimeBid,
                                            BidTotal: rsUser.BidTotal + PrPriceStep,
                                            BidPriceMax: bidMax.BidPriceMax
                                        }

                                        BidRepo.insert(poco).then(rsHolder => {
                                            sendMailBidUser(poco);
                                            res.json(rsHolder);
                                            
                                        }).catch(err => {
                                            res.statusCode = 500;
                                            res.json("6");
                                        });
                                    } else {
                                        sendMailBidUser(poco);
                                        res.json(rsUser);
                                    }
                                }).catch(err => {
                                    res.statusCode = 500;
                                    res.json("7");
                                });
                            }
                            // TH3: PriceMax của người giữ giá < người dùng
                            if (bidMaxPriceMax < BidPriceMax) {
                                // Insert cho người giữ giá
                                var poco = {
                                    BidUserID: bidMax.BidUserID,
                                    BidPrID: bidMax.BidPrID,
                                    BidTimeBid: BidTimeBid,
                                    BidTotal: bidMaxPriceMax,
                                    BidPriceMax: bidMaxPriceMax
                                }
                                BidRepo.insert(poco).then(rsHolder => {
                                    // Insert cho người dùng
                                    if (BidPriceMax >= (rsHolder.BidTotal + PrPriceStep)) {
                                        var poco = {
                                            BidUserID: BidUserID,
                                            BidPrID: BidPrID,
                                            BidTimeBid: BidTimeBid,
                                            BidTotal: rsHolder.BidTotal + PrPriceStep,
                                            BidPriceMax: BidPriceMax
                                        }

                                        BidRepo.insert(poco).then(rsUser => {
                                            sendMailBidUser(poco);
                                            res.json(rsUser);
                                            
                                        }).catch(err => {
                                            res.statusCode = 500;
                                            res.json("8");
                                        });
                                    } else if (BidPriceMax == rsHolder.BidTotal) {
                                        var poco = {
                                            BidUserID: BidUserID,
                                            BidPrID: BidPrID,
                                            BidTimeBid: BidTimeBid,
                                            BidTotal: rsHolder.BidTotal,
                                            BidPriceMax: BidPriceMax
                                        }

                                        BidRepo.insert(poco).then(rsUser => {
                                            sendMailBidUser(poco);
                                            res.json(rsUser);
                                            
                                        }).catch(err => {
                                            res.statusCode = 500;
                                            res.json("8");
                                        });
                                    } else {
                                        sendMailBidUser(poco);
                                        res.json(rsHolder);
                                    }

                                }).catch(err => {
                                    res.statusCode = 500;
                                    res.json("8");
                                });
                            }
                        }
                    }

                } else {
                    // Chưa có ai đấu giá sản phẩm này => Đây là người đấu giá đầu tiên.

                    // Nếu người này có sử dụng Auto-Bid =====> BidTotal = PrPriceOffer + PrPriceStep
                    // Không lấy giá trị BidTotal của người nhập vì thuật toán sẽ tối ưu giá cho người dùng.
                    if (BidPriceMax != null)
                        BidTotal = PrPriceOffer + PrPriceStep;

                    var poco = {
                        BidUserID: BidUserID,
                        BidPrID: BidPrID,
                        BidTimeBid: BidTimeBid,
                        BidTotal: BidTotal,
                        BidPriceMax: BidPriceMax
                    }

                    BidRepo.insert(poco).then(rs => {
                        var pocoSendMail = {
                            BidUserID: poco.BidUserID,
                            BidPrID: poco.BidPrID
                        }
                        sendMailBidUser(pocoSendMail);
                        res.json(rs);
                    }).catch(err => {
                        res.statusCode = 500;
                        res.json("9");
                    });
                }
            }).catch(err => {
                res.statusCode = 500;
                res.json("10");
            });
        }).catch(err => {
            res.statusCode = 500;
            res.json("11");
        });
    }).catch(err => {
        res.statusCode = 500;
        res.json("0");
    });
});

sendMailBidUser = function(poco){
    UserRepo.loadById({UserID: poco.BidUserID}).then(userInfo => {

        let transporter = nodemailer.createTransport({
            service: 'Gmail',
            secure: false, 
            auth: {
                user: 'quangvuong2411@gmail.com',
                pass: 'believe2259'
            }
        });
        let mailOptions = {
            from: '"Admin" <foo@example.com>', // sender address
            to: ''+ userInfo.UserEmail +'', // list of receivers
            subject: 'Thông tin đấu giá', // Subject line
            text: 'Được gửi bởi admin', // plain text body
            html: '<b>Chào '+userInfo.UserName+'</b> Bạn đã vừa ra giá thành công cho sản phẩm có ID: '+ poco.BidPrID +'' // html body
        };
        transporter.sendMail(mailOptions, (error, info) => {
            if (error) {
                return console.log(error);
            }
            console.log('Message sent: %s', info.messageId);
            // Preview only available when sending through an Ethereal account
            console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));

            // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>
            // Preview URL: https://ethereal.email/message/WaQKMgKddxQDoou...
        });

    });
}

// Update
router.put('/', (req, res) => {
    res.json("not complete");
});

// Delete by BidID
router.delete('/bid/:BidID', fnCheckToken, (req, res) => {
    if (req.params.BidID) {
        var BidID = req.params.BidID;

        if (isNaN(BidID)) {
            res.statusCode = 400;
            res.json("ID is not valid");
            return;
        }

        var poco = {
            BidID: BidID
        }

        BidRepo.deleteByBidID(poco).then(rs => {
            res.json(rs);
        }).catch(err => {
            res.statusCode = 500;
            res.json(err);
        });
    } else {
        res.statusCode = 400;
        res.json('Error');
    }
});

// Delete
router.delete('/:UserId/:PrID', fnCheckToken, (req, res) => {
    if (req.params.UserId || req.params.PrId) {
        var UserId = req.params.UserId;
        var PrId = req.params.PrID;

        if (isNaN(UserId) || isNaN(PrId)) {
            res.statusCode = 400;
            res.json("ID is not valid");
            return;
        }

        var poco = {
            BidUserID: UserId,
            BidPrID: PrId
        }

        BidRepo.delete(poco).then(rs => {
            res.json(rs);
        }).catch(err => {
            res.statusCode = 500;
            res.json(err);
        });
    } else {
        res.statusCode = 400;
        res.json('Error');
    }
});

module.exports = router;