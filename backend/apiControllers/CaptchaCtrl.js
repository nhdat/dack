var express = require('express');
var router = express.Router();
var axios = require('axios');

var secret_key = "6Le2WFwUAAAAADXQCdUvRdKDERugrMy8Dd5zAENR";
router.post('/', function(req, res) {
    var captcha_res = req.body.captcha_res;

    var url = `https://www.google.com/recaptcha/api/siteverify?secret=${secret_key}&response=${captcha_res}`;
    axios.post(url, {}, {
            headers: {
                "Content-Type": "application/x-www-form-urlencoded; charset=utf-8"
            }
        })
        .then(function(response) {
            res.json(response.data);
        })
        .catch(function(error) {
            res.json(error);
        });
});

module.exports = router;