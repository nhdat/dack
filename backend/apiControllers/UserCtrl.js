var UserRepo = require('../repos/UserRepo.js');

var express = require('express');
var router = express.Router();
var md5 = require('md5');
var nodemailer = require('nodemailer');
var limit = require('../fn/constant.js').limit;
var fnCheckToken = require('../fn/function.js').fnCheckToken;

// Load All
router.get('/', (req, res) => {
    UserRepo.loadAll().then(rows => {
        res.json(rows);
    }).catch(err => {
        res.statusCode = 500;
        res.json(err);
    });
});

// Load By page seller
router.get('/seller/:page', fnCheckToken, (req, res) => {
    var UserLevel = req.payload.UserLevel;
    if (UserLevel != 2) {
        res.json({
            msg: "error level"
        });
        return;
    }

    var page = req.params.page;
    if (isNaN(page)) {
        res.statusCode = 400;
        res.json('Page is not valid');
        return;
    }

    var poco = {
        page: page
    }
    UserRepo.loadByTimePermission(poco).then(rows => {
        if (rows.length > limit) {
            rows.pop();
            var rs = {
                hasMore: "true",
                rows: rows
            }
            res.json(rs);
        } else {
            var rs = {
                hasMore: "false",
                rows: rows
            }
            res.json(rs);
        }
    }).catch(err => {
        res.statusCode = 500;
        res.json(err);
    });
});

// Load By page
router.get('/page/:page', fnCheckToken, (req, res) => {
    var UserLevel = req.payload.UserLevel;
    if (UserLevel != 2) {
        res.json({
            msg: "error level"
        });
        return;
    }

    var page = req.params.page;
    if (isNaN(page)) {
        res.statusCode = 400;
        res.json('Page is not valid');
        return;
    }

    var poco = {
        page: page
    }
    UserRepo.loadByPage(poco).then(rows => {
        if (rows.length > limit) {
            rows.pop();
            var rs = {
                hasMore: "true",
                rows: rows
            }
            res.json(rs);
        } else {
            var rs = {
                hasMore: "false",
                rows: rows
            }
            res.json(rs);
        }
    }).catch(err => {
        res.json(err);
    });
});

// Load By ID
router.get('/:id', fnCheckToken, (req, res) => {
    if (req.params.id) {
        var id = req.params.id;
        if (isNaN(id)) {
            res.statusCode = 400;
            res.end('ID is not valid.');
            return;
        }

        var poco = {
            UserID: id
        }
        UserRepo.loadById(poco).then(rows => {
            res.json(rows);
        }).catch(err => {
            res.statusCode = 500;
            res.json(err);
        });
    } else {
        res.statusCode = 400;
        res.json('Error');
    }
});

// Load By Email
router.get('/email/:email', (req, res) => {
    if (req.params.email) {
        var email = req.params.email;

        var poco = {
            UserEmail: email
        }

        UserRepo.loadByEmail(poco).then(rows => {
            res.json(rows);
        }).catch(err => {
            res.statusCode = 500;
            res.json(err);
        });
    } else {
        res.statusCode = 400;
        res.json('Error');
    }
});

// Insert
router.post('/', (req, res) => {
    if (req.body.UserName) {
        // Check unique email
        var poco = {
            UserEmail: req.body.UserEmail
        }
        UserRepo.loadByEmail(poco).then(rows => {
            if (rows.length > 0) {
                res.statusCode = 400;
                res.json({
                    msg: "not unique email"
                });
            } else {
                // Else if email is unique email.
                var poco = {
                    UserName: req.body.UserName,
                    UserEmail: req.body.UserEmail,
                    UserPwd: md5(req.body.UserPwd),
                    UserAdd: req.body.UserAdd
                }

                UserRepo.insert(poco).then(rs => {
                    res.json(rs);
                }).catch(err => {
                    res.statusCode = 500;
                    res.json(err);
                });
            }
        }).catch(err => {
            res.statusCode = 500;
            res.json(err);
        });


    } else {
        res.statusCode = 400;
        res.json('Error');
    }
});

// User request to seller
router.post('/requestseller', fnCheckToken, (req, res) => {
    var poco = {
        UserID: req.payload.UserID
    }

    UserRepo.requestseller(poco).then(rs => {
        res.json(rs);
    }).catch(err => {
        res.statusCode = 500;
        res.json(err);
    });
});

// Update Seller
router.put('/seller', fnCheckToken, (req, res) => {
    var UserLevel = req.payload.UserLevel;
    if (UserLevel != 2) {
        res.json({
            msg: "error level"
        });
        return;
    }

    var poco = {
        UserID: req.body.UserID
    }

    UserRepo.updateSeller(poco).then(rs => {
        res.json(rs);
    }).catch(err => {
        res.statusCode = 500;
        res.json(err);
    });
});

// Update
router.put('/', fnCheckToken, (req, res) => {
    // Check unique email
    var poco = {
        UserEmail: req.body.UserEmail
    }

    UserRepo.loadByEmail(poco).then(rows => {
        var poco = {
            UserID: req.body.UserID,
            UserName: req.body.UserName,
            UserEmail: req.body.UserEmail,
            UserPwd: md5(req.body.UserPwd),
            UserAdd: req.body.UserAdd
        }

        UserRepo.update(poco).then(rs => {
            res.json(rs);
        }).catch(err => {
            res.statusCode = 500;
            res.json(err);
        });
    });
});

// Reset password
router.put('/pwd', fnCheckToken, (req, res) => {
    var UserLevel = req.payload.UserLevel;
    if (UserLevel != 2) {
        res.json({
            msg: "error level"
        });
        return;
    }

    var poco = {
        UserID: req.body.UserID,
        UserPwd: md5(req.body.UserPwd)
    }

    UserRepo.updatePwd(poco).then(rs => {
        var poco = {
            UserID: req.body.UserID,
            UserPwd: req.body.UserPwd
        }
        sendMailResetPwdUser(poco);
        res.json(rs);
    }).catch(err => {
        res.statusCode = 500;
        res.json(err);
    });
});

sendMailResetPwdUser = function(poco){
    UserRepo.loadById({UserID: poco.UserID}).then(userInfo => {

        let transporter = nodemailer.createTransport({
            service: 'Gmail',
            secure: false, 
            auth: {
                user: 'quangvuong2411@gmail.com',
                pass: 'believe2259'
            }
        });
        let mailOptions = {
            from: '"Admin" <foo@example.com>', // sender address
            to: ''+ userInfo.UserEmail +'', // list of receivers
            subject: 'Thông tin đấu giá', // Subject line
            text: 'Được gửi bởi admin', // plain text body
            html: '<b>Chào '+userInfo.UserName+'</b> Bạn đã được reset password, password mới: '+ poco.UserPwd +'' // html body
        };
        transporter.sendMail(mailOptions, (error, info) => {
            if (error) {
                return console.log(error);
            }
            console.log('Message sent: %s', info.messageId);
            // Preview only available when sending through an Ethereal account
            console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));

            // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>
            // Preview URL: https://ethereal.email/message/WaQKMgKddxQDoou...
        });

    });
}

// Check pwd
router.post('/pwd', fnCheckToken, (req, res) => {
    var poco = {
        UserID: req.body.UserID,
        UserPwd: md5(req.body.UserPwd)
    }

    UserRepo.checkPwd(poco).then(rs => {
        if(rs.length > 0){
            res.json(rs);
        }else{
            res.statusCode = 400;
            res.json("password not correct");
        }
    }).catch(err => {
        res.statusCode = 500;
        res.json(err);
    });
});

// Delete
router.delete('/:id', fnCheckToken, (req, res) => {
    var UserLevel = req.payload.UserLevel;
    if (UserLevel != 2) {
        res.json({
            msg: "error level"
        });
        return;
    }

    if (req.params.id) {
        var id = req.params.id;
        if (isNaN(id)) {
            res.statusCode = 400;
            res.json("ID is not valid");
            return;
        }

        var poco = {
            UserID: id
        }

        UserRepo.delete(poco).then(rs => {
            res.json(rs);
        }).catch(err => {
            res.statusCode = 500;
            res.json(err);
        });
    } else {
        res.statusCode = 400;
        res.json('Error');
    }
});

module.exports = router;