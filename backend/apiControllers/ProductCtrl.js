var ProductRepo = require('../repos/ProductRepo.js');
var ImgRepo = require('../repos/ImgRepo.js');
var UserRepo = require('../repos/UserRepo.js');
var BidRepo = require('../repos/BidRepo.js');
var fnCheckToken = require('../fn/function.js').fnCheckToken;
var upload = require('../fn/function.js').upload;
var convertNumber = require('../fn/function.js').convertNumber;
var formatDateTime = require('../fn/function.js').formatDateTime;
var limit = require('../fn/constant.js').limit;

var express = require('express');
var router = express.Router();

// Top 5 nhieu luot ra gia
router.get('/top5/nhieuluotragia', (req, res) => {
    ProductRepo.top5NhieuLuotRaGia().then(rows => {
        res.json(rows);
    }).catch(err => {
        res.json(err);
    });
});

// Top 5 gia cao nhat
router.get('/top5/giacaonhat', (req, res) => {
    ProductRepo.top5NhieuLuotRaGia().then(rows => {
        res.json(rows);
    }).catch(err => {
        res.json(err);
    });
});

// Top 5 gan ket thuc
router.get('/top5/ganketthuc', (req, res) => {
    ProductRepo.top5GanKetThuc().then(rows => {
        res.json(rows);
    }).catch(err => {
        res.json(err);
    });
});

// Find product without categories
router.post('/searchnotcat', (req, res) => {
    var query = req.body.query;
    var page = req.body.page;

    var poco = {
        query: query,
        page: page
    }

    ProductRepo.findProductWithoutCat(poco).then(rows => {
        if (rows.length > limit) {
            rows.pop();
            var rs = {
                hasMore: "true",
                rows: rows
            }
            res.json(rs);
        } else {
            var rs = {
                hasMore: "false",
                rows: rows
            }
            res.json(rs);
        }
    }).catch(err => {
        res.json(err);
    });
});

// Find product with categories
router.post('/searchwithcat', (req, res) => {
    var poco = {
        query: req.body.query,
        cat: req.body.cat,
        page: req.body.page
    }

    ProductRepo.findProductWithCat(poco).then(rows => {
        if (rows.length > limit) {
            rows.pop();
            var rs = {
                hasMore: "true",
                rows: rows
            }
            res.json(rs);
        } else {
            var rs = {
                hasMore: "false",
                rows: rows
            }
            res.json(rs);
        }
    }).catch(err => {
        res.json(err);
    });
});

// Load All
router.get('/', (req, res) => {
    ProductRepo.loadAll().then(rows => {
        res.json(rows);
    }).catch(err => {
        res.statusCode = 500;
        res.json(err);
    });
});

// Load By ID
router.get('/:id', (req, res) => {
    if (req.params.id) {
        var id = req.params.id;
        if (isNaN(id)) {
            res.statusCode = 400;
            res.end('ID is not valid.');
           	return;
        }

        var poco = {
            PrID: id
        }
        ProductRepo.loadById(poco).then(rows => {
            res.json(rows);
        }).catch(err => {
            res.statusCode = 500;
            res.json(err);
        });
    } else {
        res.statusCode = 400;
        res.json('Error');
    }
});

// Load By ID and return img of product
router.get('/img/:id', (req, res) => {
    if (req.params.id) {
        var id = req.params.id;
        if (isNaN(id)) {
            res.statusCode = 400;
            res.end('ID is not valid.');
            return;
        }

        var poco = {
            PrID: id
        }
        ProductRepo.loadByIdandSeller(poco).then(rows => {
            rows = rows[0];

            ProductRepo.loadBidList(poco).then(rowsBid => {
                var poco = {
                    ImgPrID: id
                }
                ImgRepo.loadByPrId(poco).then(rowsImg => {
                    var poco = {
                        UserID: rows.PrUserID
                    }
                    var rs = {
                        rows: rows,
                        rowsBid: rowsBid,
                        arrImg: rowsImg
                    }
                    res.json(rs);
                }).catch(err => {
                    res.statusCode = 500;
                    res.json(err);
                });
            }).catch(err => {
                res.statusCode = 500;
                res.json(err);
            });
        }).catch(err => {
            res.statusCode = 500;
            res.json(err);
        });
    } else {
        res.statusCode = 400;
        res.json('Error');
    }
});

// Load By page
router.get('/post/:page', fnCheckToken, (req, res) => {
    var page = req.params.page;
    if (isNaN(page)) {
        res.statusCode = 400;
        res.json('Page is not valid');
        return;
    }

    var UserID = req.payload.UserID;

    var poco = {
        page: page,
        UserID: UserID
    }

    ProductRepo.loadByPage(poco).then(rows => {
        console.log(rows.length);
        console.log(limit);

        if (rows.length > limit) {
            rows.pop();
            var rs = {
                hasMore: "true",
                rows: rows
            }
            res.json(rs);
        } else {
            var rs = {
                hasMore: "false",
                rows: rows
            }
            res.json(rs);
        }
    }).catch(err => {
        res.statusCode = 500;
        res.json(err);
    });
});

// Load By page not end
router.get('/notend/:page', fnCheckToken, (req, res) => {
    var page = req.params.page;
    if (isNaN(page)) {
        res.statusCode = 400;
        res.json('Page is not valid');
        return;
    }

    var UserID = req.payload.UserID;

    var poco = {
        page: page,
        UserID: UserID
    }

    ProductRepo.loadByPageNotEnd(poco).then(rows => {
        console.log(rows.length);
        console.log(limit);

        if (rows.length > limit) {
            rows.pop();
            var rs = {
                hasMore: "true",
                rows: rows
            }
            res.json(rs);
        } else {
            var rs = {
                hasMore: "false",
                rows: rows
            }
            res.json(rs);
        }
    }).catch(err => {
        res.statusCode = 500;
        res.json(err);
    });
});

// Load By page sold and success
router.get('/sold/:page', fnCheckToken, (req, res) => {
    var page = req.params.page;
    if (isNaN(page)) {
        res.statusCode = 400;
        res.json('Page is not valid');
        return;
    }

    var UserID = req.payload.UserID;

    var poco = {
        page: page,
        UserID: UserID
    }

    ProductRepo.loadByPageSold(poco).then(rows => {

        if (rows.length > limit) {
            rows.pop();
            var rs = {
                hasMore: "true",
                rows: rows
            }
            res.json(rs);
        } else {
            var rs = {
                hasMore: "false",
                rows: rows
            }
            res.json(rs);
        }
    }).catch(err => {
        res.statusCode = 500;
        res.json(err);
    });
});

// Load By page WatchList
router.get('/watchlist/:page', fnCheckToken, (req, res) => {
    var page = req.params.page;
    if (isNaN(page)) {
        res.statusCode = 400;
        res.json('Page is not valid');
        return;
    }

    var UserID = req.payload.UserID;

    var poco = {
        page: page,
        UserID: UserID
    }

    ProductRepo.loadByPageWatchList(poco).then(rows => {

        if (rows.length > limit) {
            rows.pop();
            var rs = {
                hasMore: "true",
                rows: rows
            }
            res.json(rs);
        } else {
            var rs = {
                hasMore: "false",
                rows: rows
            }
            res.json(rs);
        }
    }).catch(err => {
        res.statusCode = 500;
        res.json(err);
    });
});

// Load By page Bidded
router.get('/bidded/:page', fnCheckToken, (req, res) => {
    var page = req.params.page;
    if (isNaN(page)) {
        res.statusCode = 400;
        res.json('Page is not valid');
        return;
    }

    var UserID = req.payload.UserID;

    var poco = {
        page: page,
        UserID: UserID
    }

    ProductRepo.loadByPageBiddedandSuccess(poco).then(rows => {
        if (rows.length > limit) {
            rows.pop();
            var rs = {
                hasMore: "true",
                rows: rows
            }
            res.json(rs);
        } else {
            var rs = {
                hasMore: "false",
                rows: rows
            }
            res.json(rs);
        }
    }).catch(err => {
        res.statusCode = 500;
        res.json(err);
    });
});

// Load By page Bidding
router.get('/bidding/:page', fnCheckToken, (req, res) => {
    var page = req.params.page;
    if (isNaN(page)) {
        res.statusCode = 400;
        res.json('Page is not valid');
        return;
    }

    var UserID = req.payload.UserID;

    var poco = {
        page: page,
        UserID: UserID
    }

    ProductRepo.loadByPageBidding(poco).then(rows => {

        if (rows.length > limit) {
            rows.pop();
            var rs = {
                hasMore: "true",
                rows: rows
            }
            res.json(rs);
        } else {
            var rs = {
                hasMore: "false",
                rows: rows
            }
            res.json(rs);
        }
    }).catch(err => {
        res.statusCode = 500;
        res.json(err);
    });
});

// Insert
router.post('/', fnCheckToken, upload.array('imgArrCre', 3), (req, res) => {
    var PrUserID = req.payload.UserID;

    var PrCatID = req.body.sltCatCre;
    var PrName = req.body.txtNameCre;
    var PrContent = req.body.txtConentCre;
    
    var PrPriceOffer = convertNumber(req.body.txtPriceOfferCre);
    var PrPriceStep = convertNumber(req.body.txtPriceStepCre);
    var PrTimeStart = formatDateTime(req.body.txtTimeStart);
    var PrTimeEnd = formatDateTime(req.body.txtTimeEnd);
    var PrAutoReup = req.body.rdoAutoCre;

    // Optional
    var PrPriceBuyNow = req.body.txtPriceBuyNowCre;
    PrPriceBuyNow = PrPriceBuyNow == '' ? null : convertNumber(PrPriceBuyNow);

    var poco = {
        PrName : PrName,
        PrCatID: PrCatID,
        PrUserID: PrUserID,
        PrAutoReup: PrAutoReup,
        PrPriceOffer: PrPriceOffer,
        PrPriceStep: PrPriceStep,
        PrPriceBuyNow: PrPriceBuyNow,
        PrTimeStart: PrTimeStart,
        PrTimeEnd: PrTimeEnd,
        PrContent: PrContent
    }

    ProductRepo.insert(poco).then(row => {
        // Insert image
        var PrID = row.PrID;
        var files = req.files;

        for(var i = 0; i < files.length; i++){
            var poco = {
                ImgName: files[i].filename,
                ImgPrID: PrID
            }
            ImgRepo.insert(poco);
        }

        res.json(row);
    }).catch(err => {
        res.statusCode = 500;
        res.json(err);
    });
});

// Update Content
router.put('/content/:id', (req, res) => {
    if(req.params.id){
        var id = req.params.id;
        if(isNaN(id)){
            res.statusCode = 400;
            res.json('ID is not valid');
            return;
        }

        PrContentOld = req.body.PrContentOld;
        PrContentNew = req.body.PrContentNew;

        console.log(PrContentOld);
        console.log(PrContentNew);

        var poco = {
            PrID: id,
            PrContentOld: PrContentOld,
            PrContentNew: PrContentNew
        }

        ProductRepo.updateContent(poco).then(rs => {
            res.json(rs);
        }).catch(err => {
            res.statusCode = 500;
            res.json(err);
        });
    }
});

// Delete
router.delete('/:id', (req, res) => {
	if(req.params.id){
		var id = req.params.id;
		if(isNaN(id)){
			res.statusCode = 400;
			res.json("ID is not valid");
			return;
		}

		var poco = {
			PrID : id
		}
		
		ProductRepo.delete(poco).then(rs => {
			res.json(rs);
		}).catch(err => {
            res.statusCode = 500;
            res.json(err);
        });
	}else{
		res.statusCode = 400;
        res.json('Error');
	}
});

module.exports = router;