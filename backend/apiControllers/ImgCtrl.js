var ImgRepo = require('../repos/ImgRepo.js');

var express = require('express');
var router = express.Router();

// Load All
router.get('/', (req, res) => {
    ImgRepo.loadAll().then(rows => {
        res.json(rows);
    }).catch(err => {
        res.statusCode = 500;
        res.json(err);
    });
});

// Load By Product ID
router.get('/:id', (req, res) => {
    if (req.params.id) {
        var id = req.params.id;
        if (isNaN(id)) {
            res.statusCode = 400;
            res.end('ID is not valid.');
           	return;
        }

        var poco = {
            ImgPrID: id
        }
        ImgRepo.loadByPrId(poco).then(rows => {
            res.json(rows);
        }).catch(err => {
            res.statusCode = 500;
            res.json(err);
        });
    } else {
        res.statusCode = 400;
        res.json('Error');
    }
});

// Insert
router.post('/', (req, res) => {
	if(req.body.ImgPrID){
		var poco = {
			ImgName: req.body.ImgName,
        	ImgPrID: req.body.ImgPrID
		}
		
		ImgRepo.insert(poco).then(rs => {
			res.json(rs);
		}).catch(err => {
            res.statusCode = 500;
            res.json(err);
        });
	}else{
		res.statusCode = 400;
        res.json('Error');
	}
});

// Update
router.put('/', (req, res) => {
	if(req.body.ImgID){
		var poco = {
			ImgName: req.body.ImgName,
        	ImgPrID: req.body.ImgPrID,
        	ImgID: req.body.ImgID
		}
		
		ImgRepo.update(poco).then(rs => {
			res.json(rs);
		}).catch(err => {
            res.statusCode = 500;
            res.json(err);
        });
	}else{
		res.statusCode = 400;
        res.json('Error');
	}
});

// Delete
router.delete('/:id', (req, res) => {
	if(req.params.id){
		var id = req.params.id;
		if(isNaN(id)){
			res.statusCode = 400;
			res.json("ID is not valid");
			return;
		}

		var poco = {
			ImgID : id
		}
		
		ImgRepo.delete(poco).then(rs => {
			res.json(rs);
		}).catch(err => {
            res.statusCode = 500;
            res.json(err);
        });
	}else{
		res.statusCode = 400;
        res.json('Error');
	}
});

module.exports = router;