var WatchListRepo = require('../repos/WatchListRepo.js');
var fnCheckToken = require('../fn/function.js').fnCheckToken;

var express = require('express');
var router = express.Router();

// Load All
router.get('/', (req, res) => {
    WatchListRepo.loadAll().then(rows => {
        res.json(rows);
    }).catch(err => {
        res.statusCode = 500;
        res.json(err);
    });
});

// Load By User ID
router.get('/:id', (req, res) => {
    if (req.params.id) {
        var id = req.params.id;
        if (isNaN(id)) {
            res.statusCode = 400;
            res.end('ID is not valid.');
           	return;
        }

        var poco = {
            WlUserID: id
        }
        WatchListRepo.loadByUserId(poco).then(rows => {
            res.json(rows);
        }).catch(err => {
            res.statusCode = 500;
            res.json(err);
        });
    } else {
        res.statusCode = 400;
        res.json('Error');
    }
});

// Insert
router.post('/', fnCheckToken, (req, res) => {
	var poco = {
		WlUserID: req.body.WlUserID,
    	WlPrID: req.body.WlPrID
	}
	
	WatchListRepo.insert(poco).then(rs => {
		res.json(rs);
	}).catch(err => {
        res.statusCode = 500;
        res.json(err);
    });
});

// Update
router.put('/', (req, res) => {
	res.json("not complete");
});

// Delete
router.delete('/:UserId/:PrID', (req, res) => {
	if(req.params.UserId || req.params.PrId){
		var UserId = req.params.UserId;
		var PrId = req.params.PrID;

		if(isNaN(UserId) || isNaN(PrId)){
			res.statusCode = 400;
			res.json("ID is not valid");
			return;
		}

		var poco = {
			WlUserID : UserId,
			WlPrID : PrId
		}
		
		WatchListRepo.delete(poco).then(rs => {
			res.json(rs);
		}).catch(err => {
            res.statusCode = 500;
            res.json(err);
        });
	}else{
		res.statusCode = 400;
        res.json('Error');
	}
});

module.exports = router;