var UserRepo = require('../repos/UserRepo.js');

var express = require('express');
var router = express.Router();
var md5 = require('md5');

router.get('/', (req, res) => {
	var UserID = req.payload.UserID;
	var UserLevel = req.payload.UserLevel;

	if(UserLevel != 2){
		res.json({
			msg: "error level"
		});
		return;
	}

	var poco = {
		UserID : UserID
	}

	UserRepo.loadById(poco).then(rs => {
		var UserInfo = {
			UserID: rs.UserID,
			UserName: rs.UserName,
			UserEmail: rs.UserEmail
		}
        res.json(UserInfo);
    }).catch(err => {
        res.statusCode = 500;
        res.json(err);
    });
});

module.exports = router;