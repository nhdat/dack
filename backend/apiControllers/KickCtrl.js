var KickRepo = require('../repos/KickRepo.js');
var fnCheckToken = require('../fn/function.js').fnCheckToken;
var express = require('express');
var router = express.Router();
var UserRepo = require('../repos/UserRepo.js');
var nodemailer =  require('nodemailer');

// Load All
router.get('/', (req, res) => {
    KickRepo.loadAll().then(rows => {
        res.json(rows);
    }).catch(err => {
        res.statusCode = 500;
        res.json(err);
    });
});

// Load By ID
router.get('/:id', (req, res) => {
    if (req.params.id) {
        var id = req.params.id;
        if (isNaN(id)) {
            res.statusCode = 400;
            res.end('ID is not valid.');
           	return;
        }

        var poco = {
            KickUserID: id
        }
        KickRepo.loadByUserId(poco).then(rows => {
            res.json(rows);
        }).catch(err => {
            res.statusCode = 500;
            res.json(err);
        });
    } else {
        res.statusCode = 400;
        res.json('Error');
    }
});

// Insert
router.post('/', fnCheckToken, (req, res) => {
	var poco = {
		KickUserID: req.body.KickUserID,
    	KickPrID: req.body.KickPrID
	}
	
	KickRepo.insert(poco).then(rs => {
        sendMailKickUser(poco);
        res.json(rs);
    })
    .catch(err => {
        res.statusCode = 500;
        res.json(err);
    });
});

sendMailKickUser = function(poco){
    UserRepo.loadById({UserID: poco.KickUserID}).then(userInfo => {
        let transporter = nodemailer.createTransport({
            service: 'Gmail',
            secure: false, 
            auth: {
                user: 'quangvuong2411@gmail.com',
                pass: 'believe2259'
            }
        });
        let mailOptions = {
            from: '"Admin" <foo@example.com>', // sender address
            to: ''+ userInfo.UserEmail +'', // list of receivers
            subject: 'Thông tin đấu giá', // Subject line
            text: 'Được gửi bởi admin', // plain text body
            html: '<b>Chào '+userInfo.UserName+'</b> Bạn đã bị kick có sản phẩm có ID: '+ poco.KickPrID +'' // html body
        };
        transporter.sendMail(mailOptions, (error, info) => {
            if (error) {
                return console.log(error);
            }
            console.log('Message sent: %s', info.messageId);
            // Preview only available when sending through an Ethereal account
            console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));

            // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>
            // Preview URL: https://ethereal.email/message/WaQKMgKddxQDoou...
        });

    })
}

// Update
router.put('/', (req, res) => {
	res.json("not complete");
});

// Delete
router.delete('/:UserId/:PrID', (req, res) => {
	if(req.params.UserId || req.params.PrId){
		var UserId = req.params.UserId;
		var PrId = req.params.PrID;

		if(isNaN(UserId) || isNaN(PrId)){
			res.statusCode = 400;
			res.json("ID is not valid");
			return;
		}

		var poco = {
			KickUserID : UserId,
			KickPrID : PrId
		}
		
		KickRepo.delete(poco).then(rs => {
			res.json(rs);
		}).catch(err => {
            res.statusCode = 500;
            res.json(err);
        });
	}else{
		res.statusCode = 400;
        res.json('Error');
	}
});

module.exports = router;