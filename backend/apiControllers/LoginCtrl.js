var UserRepo = require('../repos/UserRepo.js');

var express = require('express');
var router = express.Router();
var md5 = require('md5');
var jwt = require('jsonwebtoken');

var secret_key = "dack";

// Login
router.post('/', (req, res) => {
    var email = req.body.UserEmail;
    var pwd = req.body.UserPwd;
    var poco = {
        UserEmail: email,
        UserPwd: md5(pwd)
    }

    UserRepo.loadByEmailaPwd(poco).then(rows => {
        if (rows.length > 0) {
            var payload = {
                UserID: rows[0].UserID,
                UserLevel: rows[0].UserLevel,
                UserEmail: rows[0].UserEmail
            }

            var token = jwt.sign(payload, secret_key, {
                expiresIn: '1days'
            });

            res.json({
                access_token: token,
                UserID: rows[0].UserID,
                UserEmail: rows[0].UserEmail
            });
        } else {
            res.statusCode = 400;
            res.json("fail token");
        }
    }).catch(err => {
        res.statusCode = 500;
        res.json(err);
    });
});

module.exports = router;