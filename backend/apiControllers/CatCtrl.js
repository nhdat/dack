var CatRepo = require('../repos/CatRepo.js');
var fnCheckToken = require('../fn/function.js').fnCheckToken;
var express = require('express');
var router = express.Router();

var constant = require('../fn/constant.js');
const limit = constant.limit;

// Load All
router.get('/', (req, res) => {
    CatRepo.loadAll().then(rows => {
        res.json(rows);
    }).catch(err => {
        res.statusCode = 500;
        res.json(err);
    });
});

// Load By Page
router.get('/page/:page', (req, res) => {
	var page = req.params.page;
	if(isNaN(page)){
		res.statusCode = 400;
		res.json('Page is not valid');
		return;
	}

	var poco = {
		page : page
	}
	CatRepo.loadByPage(poco).then(rows => {
		if(rows.length > limit){
			rows.pop();
			var rs = {
				hasMore : "true",
				rows : rows
			}
			res.json(rs);
		}else{
			var rs = {
				hasMore : "false",
				rows : rows
			}
			res.json(rs);
		}
	}).catch(err => {
		res.statusCode = 500;
		res.json(err);
	});
})

// Load By ID
router.get('/:id', (req, res) => {
    if (req.params.id) {
        var id = req.params.id;
        if (isNaN(id)) {
            res.statusCode = 400;
            res.end('ID is not valid.');
           	return;
        }

        var poco = {
            CatID: id
        }
        CatRepo.loadById(poco).then(rows => {
            res.json(rows);
        }).catch(err => {
            res.statusCode = 500;
            res.json(err);
        });
    } else {
        res.statusCode = 400;
        res.json('Error');
    }
});

// Insert
router.post('/', fnCheckToken, (req, res) => {
	var UserID = req.payload.UserID;
	var UserLevel = req.payload.UserLevel;

	if(UserLevel != 2){
		res.json({
			msg: "error level"
		});
		return;
	}

	if(req.body.CatName){
		var poco = {
			CatName : req.body.CatName
		}
		
		CatRepo.insert(poco).then(rs => {
			res.json(rs);
		}).catch(err => {
            res.statusCode = 500;
            res.json(err);
        });
	}else{
		res.statusCode = 400;
        res.json('Error');
	}
});

// Update
router.put('/', fnCheckToken, (req, res) => {
	var UserID = req.payload.UserID;
	var UserLevel = req.payload.UserLevel;

	if(UserLevel != 2){
		res.json({
			msg: "error level"
		});
		return;
	}

	if(req.body.CatName && req.body.CatID){
		var poco = {
			CatID : req.body.CatID,
			CatName : req.body.CatName
		}
		
		CatRepo.update(poco).then(rs => {
			res.json(rs);
		}).catch(err => {
            res.statusCode = 500;
            res.json(err);
        });
	}else{
		res.statusCode = 400;
        res.json('Error');
	}
});

// Delete
router.delete('/:id', fnCheckToken, (req, res) => {
	var UserID = req.payload.UserID;
	var UserLevel = req.payload.UserLevel;

	if(UserLevel != 2){
		res.json({
			msg: "error level"
		});
		return;
	}
	
	if(req.params.id){
		var id = req.params.id;
		if(isNaN(id)){
			res.statusCode = 400;
			res.json("ID is not valid");
			return;
		}

		var poco = {
			CatID : id
		}
		
		CatRepo.delete(poco).then(rs => {
			res.json(rs);
		}).catch(err => {
            res.statusCode = 500;
            res.json(err);
        });
	}else{
		res.statusCode = 400;
        res.json('Error');
	}
});

module.exports = router;