var express = require('express');
var bodyparser = require('body-parser');
var cors = require('cors');
var morgan = require('morgan');
var path = require('path');
var fnCheckToken = require('./fn/function.js').fnCheckToken;
var schedule = require('node-schedule'); 
var nodemailer =  require('nodemailer');
var app = express();
app.use(morgan('dev'));
app.use(cors());
app.use(bodyparser.json());

var staticDir = express.static(
    path.resolve(__dirname, 'public')
);
app.use(staticDir);

var CatCtrl = require('./apiControllers/CatCtrl.js');
var ProductCtrl = require('./apiControllers/ProductCtrl.js');
var UserCtrl = require('./apiControllers/UserCtrl.js');
var WatchListCtrl = require('./apiControllers/WatchListCtrl.js');
var KickCtrl = require('./apiControllers/KickCtrl.js');
var ImgCtrl = require('./apiControllers/ImgCtrl.js');
var BidCtrl = require('./apiControllers/BidCtrl.js');
var LoginCtrl = require('./apiControllers/LoginCtrl.js');
var AdminCtrl = require('./apiControllers/AdminCtrl.js');
var SendMailCtrl = require('./apiControllers/SendMailCtrl.js');
var CaptchaCtrl = require('./apiControllers/CaptchaCtrl.js');

app.use('/cat', CatCtrl);
app.use('/product', ProductCtrl);
app.use('/user', UserCtrl);
app.use('/watchlist', WatchListCtrl);
app.use('/kick', KickCtrl);
app.use('/img', ImgCtrl);
app.use('/bid', BidCtrl);
app.use('/login', LoginCtrl);
app.use('/admin', fnCheckToken, AdminCtrl);
app.use('/mail',SendMailCtrl);
app.use('/captcha', CaptchaCtrl);


var formatDateTimeUser = require('./fn/function.js').formatDateTimeUser;
ProductRepo = require('./repos/ProductRepo.js');
BidRepo = require('./repos/BidRepo.js');
UserRepo = require('./repos/UserRepo.js');
var j = schedule.scheduleJob('1 * * * * *', function(){
    console.log('This line will run every 1 minute');

    var date = new Date();
    date.setMonth(date.getMonth() + 1);
    var dateCurrent = formatDateTimeUser(date);

// Kick hoat san pham.
    var poco = {
    	dateCurrent: dateCurrent
    }
    ProductRepo.findProductOff(poco).then(rs => {
    	for(var i = 0; i < rs.length; i++){
    		var pocoUpdate = {
    			PrID: rs[i].PrID
    		}
    		ProductRepo.updateProductOn(pocoUpdate).then(rs => {
    			console.log(rs);
    		})
    	}
    });

// Xử lý kết thúc sản phẩm
	var poco = {
    	dateCurrent: dateCurrent
    }
    ProductRepo.findProductTimeOut(poco).then(rs => {
    	for(var i = 0; i < rs.length; i++){
    		console.log(rs[i]);
    		var seller = rs[i];
    		// Tìm Bid max
    		var poco = {
    			BidPrID: rs[i].PrID
    		}
    		BidRepo.loadMaxBidByProductID(poco).then(bidmax => {
    			console.log(bidmax);
    			bidmax = bidmax[0];
    			if(bidmax){
    				// Send mail cho người thắng
    				poco = {
    					UserID: bidmax.UserID,
    					PrID: bidmax.BidPrID
    				}
    				sendMailWinner(poco);

    				// Send mail cho người bán
    				poco = {
    					UserID: seller.PrUserID,
    					PrID: seller.PrID
    				}
    				sendMailSeller(poco);
    			}
    			else{
    				// Send mail cho người bán (không có ai đấu giá)
    				poco = {
    					UserID: seller.PrUserID,
    					PrID: seller.PrID
    				}
    				sendMailSeller_NoBid(poco);
    			}
    		}).catch(err => {
    			console.log(err);
    		});
    	}
    });


// Gia hạn sản phẩm
	poco = {
    	dateCurrent: dateCurrent
    }
    ProductRepo.findProductReup(poco).then(rows => {
    	for(var i = 0; i < rows.length; i++){
    		var value = rows[i];
    		var poco = {
    			BidPrID: rows[i].PrID,
    			PrTimeEnd: rows[i].PrTimeEnd 
    		}
    		ProductRepo.findUserBidReup(poco).then(users => {
    			if(users.length > 0){
    				// Gia han san pham
    				var poco = {
    					PrTimeEnd: value.PrTimeEnd,
    					PrID: value.PrID
    				}
    				ProductRepo.updateProductReup(poco).spread((results, metadata) => {
    					console.log(results);
    					console.log(metadata);
    				});
    			}
    		});
    	}
    });
});

app.get('/', (req, res) => {
    res.json('Server running');
});

app.listen('6500', function(){
    console.log('Server running on port 6500');
});

sendMailWinner = function(poco){
    UserRepo.loadById({UserID: poco.UserID}).then(userInfo => {
        let transporter = nodemailer.createTransport({
            service: 'Gmail',
            secure: false, 
            auth: {
                user: 'quangvuong2411@gmail.com',
                pass: 'believe2259'
            }
        });
        let mailOptions = {
            from: '"Admin" <foo@example.com>', // sender address
            to: ''+ userInfo.UserEmail +'', // list of receivers
            subject: 'Thông tin đấu giá', // Subject line
            text: 'Được gửi bởi admin', // plain text body
            html: '<b>Chào '+userInfo.UserName+'</b> Bạn đã chiến thắng đấu giá sản phẩm có ID: '+ poco.PrID +'' // html body
        };
        transporter.sendMail(mailOptions, (error, info) => {
            if (error) {
                return console.log(error);
            }
            console.log('Message sent: %s', info.messageId);
            // Preview only available when sending through an Ethereal account
            console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));

            // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>
            // Preview URL: https://ethereal.email/message/WaQKMgKddxQDoou...
        });

    })
}

sendMailSeller = function(poco){
    UserRepo.loadById({UserID: poco.UserID}).then(userInfo => {
        let transporter = nodemailer.createTransport({
            service: 'Gmail',
            secure: false, 
            auth: {
                user: 'quangvuong2411@gmail.com',
                pass: 'believe2259'
            }
        });
        let mailOptions = {
            from: '"Admin" <foo@example.com>', // sender address
            to: ''+ userInfo.UserEmail +'', // list of receivers
            subject: 'Thông tin đấu giá', // Subject line
            text: 'Được gửi bởi admin', // plain text body
            html: '<b>Chào '+userInfo.UserName+'</b> Đã có người đấu giá thắng sản phẩm có ID: '+ poco.PrID + ' của bạn.' // html body
        };
        transporter.sendMail(mailOptions, (error, info) => {
            if (error) {
                return console.log(error);
            }
            console.log('Message sent: %s', info.messageId);
            // Preview only available when sending through an Ethereal account
            console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));

            // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>
            // Preview URL: https://ethereal.email/message/WaQKMgKddxQDoou...
        });

    })
}

sendMailSeller_NoBid = function(poco){
    UserRepo.loadById({UserID: poco.UserID}).then(userInfo => {
        let transporter = nodemailer.createTransport({
            service: 'Gmail',
            secure: false, 
            auth: {
                user: 'quangvuong2411@gmail.com',
                pass: 'believe2259'
            }
        });
        let mailOptions = {
            from: '"Admin" <foo@example.com>', // sender address
            to: ''+ userInfo.UserEmail +'', // list of receivers
            subject: 'Thông tin đấu giá', // Subject line
            text: 'Được gửi bởi admin', // plain text body
            html: '<b>Chào '+userInfo.UserName+'</b> Không ai đấu giá sản phẩm có ID: '+ poco.PrID + ' của bạn.' // html body
        };
        transporter.sendMail(mailOptions, (error, info) => {
            if (error) {
                return console.log(error);
            }
            console.log('Message sent: %s', info.messageId);
            // Preview only available when sending through an Ethereal account
            console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));

            // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>
            // Preview URL: https://ethereal.email/message/WaQKMgKddxQDoou...
        });

    })
}