const Sequelize = require('sequelize');

const sequelize = new Sequelize('dack', 'root', '', {
    host: 'localhost',
    dialect: 'mysql',
    operatorsAliases: false,

    // TimeZone
    timezone: "+07:00",
    dialectOptions: {
        useUTC: true, //for reading from database
        dateStrings: true,

        typeCast: function(field, next) { // for reading from database
            if (field.type === 'DATETIME') {
                return field.string();
            }
            return next();
        },
    },

    pool: {
        max: 5,
        min: 0,
        acquire: 30000,
        idle: 10000
    }
});

module.exports = {
    bid: sequelize.import('./models/bid'),
    cat: sequelize.import('./models/cat'),
    img: sequelize.import('./models/img'),
    kick: sequelize.import('./models/kick'),
    product: sequelize.import('./models/product'),
    user: sequelize.import('./models/user'),
    watchlist: sequelize.import('./models/watchlist'),
    Op: Sequelize.Op,
    sequelize: sequelize
}