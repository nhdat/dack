var page = 1;

$('#btnCat').click(function() {
    window.location.replace("http://localhost:8080/admin");
});

$('#btnUser').click(function() {
    window.location.replace("http://localhost:8080/admin/user");
});

$(function() {
    verifyToken();
    $('#btnSeller').trigger('click');
});

function verifyToken() {
    $.ajax({
        url: 'http://localhost:6500/admin',
        dataType: 'json',
        timeout: 10000,
        type: 'GET',
        headers: {
            'x-access-token': localStorage.access_token
        }
    }).done(function(data) {
        console.log(data);
        if (data.msg === "error level") {
            $('#LoginModal').modal('show');
            swal("Thông Báo", "Tài khoản không đủ quyền", "warning");
            $('#LoginModal').modal('show');
        } else {
            //swal("Thông Báo", "Đăng nhập thành công", "success");
            $('#txtUserName').text(data.UserName);
        }
    }).catch(function(xhr, textStatus, error) {
        console.log(xhr.responseJSON.msg);
        console.log(textStatus);
        console.log(error);

        if (xhr.responseJSON.msg === 'verify fail') {
            $('#LoginModal').modal('show');
            swal("Thông Báo", "Token hết hạn, vui lòng đăng nhập lại.", "warning");
            $('#LoginModal').modal('show');
        } else if (xhr.responseJSON.msg === 'no token found') {
            $('#LoginModal').modal('show');
            swal("Thông Báo", "Không tìm thấy token, vui lòng đăng nhập.", "warning");
            $('#LoginModal').modal('show');
        } else {
            $('#LoginModal').modal('show');
        }
    });
}

// If Login Modal open. Focus Email input.
$('#LoginModal').on('shown.bs.modal', function() {
    $('#txtEmail').focus();
});

// Click button login on modal login
$('#btnLogin').click(function() {
    var isValid = $('#frmLogin').valid();
    if (isValid) {
        var email = $('#txtEmail').val();
        var pwd = $('#txtPwd').val();

        var poco = {
            UserEmail: email,
            UserPwd: pwd
        }

        saveToken(poco);
    }
});

// Function save token to local storage
function saveToken(poco) {
    $.ajax({
        url: "http://localhost:6500/login",
        dataType: 'json',
        timeout: 10000,
        type: 'POST',
        contentType: 'application/json',
        data: JSON.stringify(poco)
    }).done(function(data) {
        // Cấp phát token thành công.
        localStorage.access_token = data.access_token;
        console.log(data);
        $('#LoginModal').modal('toggle');

        location.reload();
    }).fail(function(xhr, textStatus, err) {
        console.log(xhr);
        console.log(textStatus);
        console.log(err);

        swal("Thông Báo", "Email hoặc mật khẩu không đúng.", "warning");
    });
}

// LogOut
$('#btnLogout').click(function() {
    localStorage.clear();
    $('#LoginModal').modal('show');
});

///////////////////////////////////////////////////// Event of seller
$('#btnSeller').click(function() {
    $('#seller-list').empty();
    page = 1;
    loadSellerMore();
});

// Function load Seller More
function loadSellerMore() {
    $('#loader').show();
    $.ajax({
        url: 'http://localhost:6500/user/seller/' + page,
        dataType: 'json',
        timeout: 10000,
        headers: {
            'x-access-token': localStorage.access_token
        }
    }).then(function(rs) {
        console.log(rs);

        var hasMore = rs.hasMore;
        var rows = rs.rows;

        for (var i = 0; i < rows.length; i++) {
            var datetime = rows[i].UserTimePermission;
            var split = datetime.split(' ');
            var date = split[0];
            var time = split[1];

            //console.log(date);
            var splitdate = date.split('-');
            var year = splitdate[0];
            var month = splitdate[1];
            var day = splitdate[2];

            //console.log(time);
            splittime = time.split(':');
            var hours = splittime[0];
            var minute = splittime[1];
            var second = splittime[2];

            var str = day + "/" + month + "/" + year + " (" + hours + ":" + minute + ":" + second +")";
            rows[i].UserTimePermission = str;
        }

        var source = $('#seller-template').html();
        var template = Handlebars.compile(source);
        var html = template(rows);
        $('#seller-list').append(html);

        $('#seller-list tr[style]').fadeIn(1000, function() {
            $(this).removeAttr('style');
        });

        page++;
        if (hasMore == "false") {
            $('#btnLoadSellerMore').hide();
        } else {
            $('#btnLoadSellerMore').show();
        }

        $('#loader').hide();

    }).catch(function(xhr, textStatus, error) {
        console.log(xhr);
        console.log(textStatus);
        console.log(error);

        if (xhr.responseJSON.msg === 'verify fail') {
            $('#LoginModal').modal('show');
            swal("Thông Báo", "Token hết hạn, vui lòng đăng nhập lại.", "warning");
            $('#LoginModal').modal('show');
        } else if (xhr.responseJSON.msg === 'no token found') {
            $('#LoginModal').modal('show');
            swal("Thông Báo", "Không tìm thấy token, vui lòng đăng nhập.", "warning");
            $('#LoginModal').modal('show');
        } else {
            $('#LoginModal').modal('show');
        }
    });
}

// Click button load seller more
$('#btnLoadSellerMore').click(function() {
    loadSellerMore();
});

$('#seller-list').on('click', '.btnSellerEdit', function() {
	var tr = $(this).closest('tr');
	var id = $(this).data('id');

    swal({
        title: "Bạn có muốn duyệt ?",
        text: "Khi duyệt sẽ không thể khôi phục.",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    }).then((willEdit) => {
        if(willEdit){
        	var dt = new Date();
        	dt.setDate(dt.getDate() + 7);

        	var year = dt.getFullYear();
        	var month = dt.getMonth() + 1;
        	var day = dt.getDate();

        	var output = year + '-' + (month<10 ? '0' : '') + month + '-' + (day<10 ? '0' : '') + day;

        	var poco = {
        		UserID : id,
        		UserTimeEndSeller: output
        	}

        	$.ajax({
        		url: 'http://localhost:6500/user/seller',
        		dataType: 'json',
        		contentType: 'application/json',
        		type: 'PUT',
        		data: JSON.stringify(poco),
        		headers: {
        			'x-access-token' : localStorage.access_token
        		}
        	}).done(function(data){
        		if(data == 1){
        			tr.fadeOut('slow', function() {
                        swal("Duyệt thành công.", {
                            icon: "success",
                        });
                        tr.remove();
                    });
        		}
        	}).catch(function(xhr, textStatus, err){
        		console.log(xhr);
        		console.log(textStatus);
        		console.log(err);
        	});
        }
    });
});