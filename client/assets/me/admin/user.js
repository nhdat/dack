var page = 1;

$('#btnCat').click(function() {
    window.location.replace("http://localhost:8080/admin");
});

$('#btnSeller').click(function() {
    window.location.replace("http://localhost:8080/admin/seller");
});

$(function() {
    verifyToken();
    $('#btnUser').trigger('click');
});

function verifyToken() {
    $.ajax({
        url: 'http://localhost:6500/admin',
        dataType: 'json',
        timeout: 10000,
        type: 'GET',
        headers: {
            'x-access-token': localStorage.access_token
        }
    }).done(function(data) {
        console.log(data);
        if (data.msg === "error level") {
            $('#LoginModal').modal('show');
            swal("Thông Báo", "Tài khoản không đủ quyền", "warning");
            $('#LoginModal').modal('show');
        } else {
            //swal("Thông Báo", "Đăng nhập thành công", "success");
            $('#txtUserName').text(data.UserName);
        }
    }).catch(function(xhr, textStatus, error) {
        console.log(xhr.responseJSON.msg);
        console.log(textStatus);
        console.log(error);

        if (xhr.responseJSON.msg === 'verify fail') {
            $('#LoginModal').modal('show');
            swal("Thông Báo", "Token hết hạn, vui lòng đăng nhập lại.", "warning");
            $('#LoginModal').modal('show');
        } else if (xhr.responseJSON.msg === 'no token found') {
            $('#LoginModal').modal('show');
            swal("Thông Báo", "Không tìm thấy token, vui lòng đăng nhập.", "warning");
            $('#LoginModal').modal('show');
        } else {
            $('#LoginModal').modal('show');
        }
    });
}

// If Login Modal open. Focus Email input.
$('#LoginModal').on('shown.bs.modal', function() {
    $('#txtEmail').focus();
});

// Click button login on modal login
$('#btnLogin').click(function() {
    var isValid = $('#frmLogin').valid();
    if (isValid) {
        var email = $('#txtEmail').val();
        var pwd = $('#txtPwd').val();

        var poco = {
            UserEmail: email,
            UserPwd: pwd
        }

        saveToken(poco);
    }
});

// Function save token to local storage
function saveToken(poco) {
    $.ajax({
        url: "http://localhost:6500/login",
        dataType: 'json',
        timeout: 10000,
        type: 'POST',
        contentType: 'application/json',
        data: JSON.stringify(poco)
    }).done(function(data) {
        // Cấp phát token thành công.
        localStorage.access_token = data.access_token;
        console.log(data);
        $('#LoginModal').modal('toggle');

        location.reload();
    }).fail(function(xhr, textStatus, err) {
        console.log(xhr);
        console.log(textStatus);
        console.log(err);

        swal("Thông Báo", "Email hoặc mật khẩu không đúng.", "warning");
    });
}

// LogOut
$('#btnLogout').click(function() {
    localStorage.clear();
    $('#LoginModal').modal('show');
});

///////////////////////////////////////////////////// Event of User
$('#btnUser').click(function() {
    $('#user-list').empty();
    page = 1;
    loadUserMore();
});

// Function load Seller More ----------------------------> Edit
function loadUserMore() {
    $('#loader').show();
    $.ajax({
        url: 'http://localhost:6500/user/page/' + page,
        dataType: 'json',
        timeout: 10000,
        headers: {
            'x-access-token': localStorage.access_token
        }
    }).then(function(rs) {
        console.log(rs);

        var hasMore = rs.hasMore;
        var rows = rs.rows;

        var source = $('#user-template').html();
        var template = Handlebars.compile(source);
        var html = template(rows);
        $('#user-list').append(html);

        $('#user-list tr[style]').fadeIn(1000, function() {
            $(this).removeAttr('style');
        });

        page++;
        if (hasMore == "false") {
            $('#btnLoadUserMore').hide();
        } else {
            $('#btnLoadUserMore').show();
        }

        $('#loader').hide();

    }).catch(function(xhr, textStatus, error) {
        console.log(xhr);
        console.log(textStatus);
        console.log(error);

        if (xhr.responseJSON.msg === 'verify fail') {
            $('#LoginModal').modal('show');
            swal("Thông Báo", "Token hết hạn, vui lòng đăng nhập lại.", "warning");
            $('#LoginModal').modal('show');
        } else if (xhr.responseJSON.msg === 'no token found') {
            $('#LoginModal').modal('show');
            swal("Thông Báo", "Không tìm thấy token, vui lòng đăng nhập.", "warning");
            $('#LoginModal').modal('show');
        } else {
            $('#LoginModal').modal('show');
        }
    });
}

// Click button load seller more
$('#btnLoadUserMore').click(function() {
    loadUserMore();
});

// Event Reset password
$('#user-list').on('click', '.btnUserResetPwd', function() {
    var tr = $(this).closest('tr');
    var id = $(this).data('id');

    swal({
        title: "Bạn có muốn reset password ?",
        text: "Khi reset sẽ không thể khôi phục.",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    }).then((willReset) => {
        var pwd = Math.random().toString(36).substring(2);

        if (willReset) {
            var poco = {
                UserID: id,
                UserPwd: pwd
            }

            $.ajax({
                url: 'http://localhost:6500/user/pwd',
                type: 'PUT',
                dataType: 'json',
                contentType: 'application/json',
                timeout: 10000,
                data: JSON.stringify(poco),
                headers: {
                    'x-access-token': localStorage.access_token
                }
            }).done(function(data) {
                if(data == 1){
                    swal("Thông Báo", "Reset password thành công. Đã gửi email tới chủ tài khoản.", "success");
                }
            }).catch(function(xhr, textStatus, error) {
                console.log(xhr);
                console.log(textStatus);
                console.log(error);

                if (xhr.responseJSON.msg === 'verify fail') {
                    $('#LoginModal').modal('show');
                    swal("Thông Báo", "Token hết hạn, vui lòng đăng nhập lại.", "warning");
                    $('#LoginModal').modal('show');
                } else if (xhr.responseJSON.msg === 'no token found') {
                    $('#LoginModal').modal('show');
                    swal("Thông Báo", "Không tìm thấy token, vui lòng đăng nhập.", "warning");
                    $('#LoginModal').modal('show');
                } else {
                    $('#LoginModal').modal('show');
                }
            });
        }
    });
});

// Event Delete User
$('#user-list').on('click', '.btnUserDel', function() {
    var tr = $(this).closest('tr');
    var id = $(this).data('id');

    swal({
        title: "Bạn có muốn xóa tài khoản ?",
        text: "Khi xóa sẽ không thể khôi phục.",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    }).then((willReset) => {

        if (willReset) {
            var poco = {
                UserID: id
            }

            $.ajax({
                url: 'http://localhost:6500/user/' + id,
                type: 'DELETE',
                dataType: 'json',
                timeout: 10000,
                headers: {
                    'x-access-token': localStorage.access_token
                }
            }).done(function(data) {
                if(data == 1){
                    tr.fadeOut('slow', function() {
                        swal("Xóa thành công.", {
                            icon: "success",
                        });
                        tr.remove();
                    });
                }
            }).catch(function(xhr, textStatus, error) {
                console.log(xhr);
                console.log(textStatus);
                console.log(error);

                if (xhr.responseJSON.msg === 'verify fail') {
                    $('#LoginModal').modal('show');
                    swal("Thông Báo", "Token hết hạn, vui lòng đăng nhập lại.", "warning");
                    $('#LoginModal').modal('show');
                } else if (xhr.responseJSON.msg === 'no token found') {
                    $('#LoginModal').modal('show');
                    swal("Thông Báo", "Không tìm thấy token, vui lòng đăng nhập.", "warning");
                    $('#LoginModal').modal('show');
                } else {
                    $('#LoginModal').modal('show');
                }
            });
        }
    });
});