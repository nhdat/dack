var page = 1;

$('#btnSeller').click(function(){
    window.location.replace("http://localhost:8080/admin/seller");
});

$('#btnUser').click(function(){
    window.location.replace("http://localhost:8080/admin/user");
});


$(function() {
    verifyToken();
    $('#btnCat').trigger('click');
});

function verifyToken() {
    $.ajax({
        url: 'http://localhost:6500/admin',
        dataType: 'json',
        timeout: 10000,
        type: 'GET',
        headers: {
            'x-access-token': localStorage.access_token
        }
    }).done(function(data) {
        console.log(data);
        if (data.msg === "error level") {
            $('#LoginModal').modal('show');
            swal("Thông Báo", "Tài khoản không đủ quyền", "warning");
            $('#LoginModal').modal('show');
        } else {
            //swal("Thông Báo", "Đăng nhập thành công", "success");
            $('#txtUserName').text(data.UserName);
        }
    }).catch(function(xhr, textStatus, error) {
        console.log(xhr.responseJSON.msg);
        console.log(textStatus);
        console.log(error);

        if (xhr.responseJSON.msg === 'verify fail') {
            $('#LoginModal').modal('show');
            swal("Thông Báo", "Token hết hạn, vui lòng đăng nhập lại.", "warning");
            $('#LoginModal').modal('show');
        } else if (xhr.responseJSON.msg === 'no token found') {
            $('#LoginModal').modal('show');
            swal("Thông Báo", "Không tìm thấy token, vui lòng đăng nhập.", "warning");
            $('#LoginModal').modal('show');
        } else {
            $('#LoginModal').modal('show');
        }
    });
}

// If Login Modal open. Focus Email input.
$('#LoginModal').on('shown.bs.modal', function() {
    $('#txtEmail').focus();
});

// Validation form Login
$('#frmLogin').validate({
    rules: {
        email: {
            required: true,
            email: true
        },
        password: {
            required: true,
            minlength: 6
        }
    },
    messages: {
        email: {
            required: 'Nhập email',
            email: 'Nhập email đúng định dạng'
        },
        password: {
            required: "Chưa nhập mật khẩu.",
            minlength: "Mật khẩu phải nhiều hơn 6 ký tự."
        }
    },

    highlight: function(element) { // hightlight error inputs
        $(element)
            .closest('div')
            .addClass('has-error'); // set error class to the control group
    },

    success: function(label) {
        label.closest('div').removeClass('has-error');
        label.remove();
    },

    errorElement: 'span',
    errorClass: 'help-block'
});

// Click button login on modal login
$('#btnLogin').click(function() {
    var isValid = $('#frmLogin').valid();
    if (isValid) {
        var email = $('#txtEmail').val();
        var pwd = $('#txtPwd').val();

        var poco = {
            UserEmail: email,
            UserPwd: pwd
        }

        saveToken(poco);
    }
});

// Function save token to local storage
function saveToken(poco) {
    $.ajax({
        url: "http://localhost:6500/login",
        dataType: 'json',
        timeout: 10000,
        type: 'POST',
        contentType: 'application/json',
        data: JSON.stringify(poco)
    }).done(function(data) {
        // Cấp phát token thành công.
        localStorage.access_token = data.access_token;
        console.log(data);
        $('#LoginModal').modal('toggle');

        location.reload();
    }).fail(function(xhr, textStatus, err) {
        console.log(xhr);
        console.log(textStatus);
        console.log(err);

        swal("Thông Báo", "Email hoặc mật khẩu không đúng.", "warning");
    });
}

// LogOut
$('#btnLogout').click(function() {
    localStorage.clear();
    $('#LoginModal').modal('show');
});


/////////////////////////////////////////////////// Event of Categories
// Click list - group Cat
$('#btnCat').click(function() {
    $('#cat-list').empty();
    page = 1;
    loadCatMore();
});

// Click button load cat more
$('#btnLoadCatMore').click(function() {
    loadCatMore();
});

// Function load Cat More
function loadCatMore() {
    $('#loader').show();
    $.ajax({
        url: 'http://localhost:6500/cat/page/' + page,
        dataType: 'json',
        timeout: 10000
    }).then(function(rs) {
        console.log(rs);

        var hasMore = rs.hasMore;
        var rows = rs.rows;

        var source = $('#cat-template').html();
        var template = Handlebars.compile(source);
        var html = template(rows);
        $('#cat-list').append(html);

        $('#cat-list tr[style]').fadeIn(1000, function() {
            $(this).removeAttr('style');
        });

        page++;
        if (hasMore == "false") {
            $('#btnLoadCatMore').hide();
        } else {
            $('#btnLoadCatMore').show();
        }

        $('#loader').hide();

    }).catch(function(xhr, textStatus, error) {
        console.log(xhr.responseJSON);
        console.log(textStatus);
        console.log(error);

        $('#LoginModal').modal('show');
    });
}

/////////////////////////////////////////////////////// Add new, delete, edit cat //////////////////////////////////////////////
// Click button Del Cat-List
$('#cat-list').on('click', '.btnCatDel', function() {

    var tr = $(this).closest('tr');
    var id = $(this).data('id');

    swal({
        title: "Bạn có muốn xóa ?",
        text: "Khi xóa sẽ không thể khôi phục.",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    }).then((willDelete) => {
        if (willDelete) {
            $.ajax({
                url: 'http://localhost:6500/cat/' + id,
                dataType: 'json',
                type: 'DELETE',
                contentType: 'application/json',
                timeout: 10000,
                headers: {
                    'x-access-token': localStorage.access_token
                }
            }).done(function(data) {
                if (data.msg === "error level") {
                    $('#LoginModal').modal('show');
                    swal("Thông Báo", "Tài khoản không đủ quyền", "warning");
                    $('#LoginModal').modal('show');
                }

                if (data === 1) {
                    tr.fadeOut('slow', function() {
                        swal("Xóa thành công.", {
                            icon: "success",
                        });
                        tr.remove();
                    });
                } else {
                    swal("Thông Báo", "Có lỗi xảy ra", "warning");
                }
            }).catch(function(xhr, textStatus, error) {
                console.log(xhr);
                console.log(textStatus);
                console.log(error);

                if (xhr.responseJSON.msg === 'verify fail') {
                    $('#LoginModal').modal('show');
                    swal("Thông Báo", "Token hết hạn, vui lòng đăng nhập lại.", "warning");
                    $('#LoginModal').modal('show');
                } else if (xhr.responseJSON.msg === 'no token found') {
                    $('#LoginModal').modal('show');
                    swal("Thông Báo", "Không tìm thấy token, vui lòng đăng nhập.", "warning");
                    $('#LoginModal').modal('show');
                } else {
                    $('#LoginModal').modal('show');
                }
            });
        }
    });
});

$('#btnCatAdd').click(function() {
    $('#CatAddModal').modal('show');
});

// If Cat Add Modal open. Focus Cat Name input.
$('#CatAddModal').on('shown.bs.modal', function() {
    $('#txtCatName').focus();
});

// Validate form cat add
$('#frmCatAdd').validate({
    rules: {
        txtCatName: {
            required: true
        }
    },
    messages: {
        txtCatName: {
            required: 'Nhập tên chuyên mục'
        }
    },

    highlight: function(element) { // hightlight error inputs
        $(element)
            .closest('div')
            .addClass('has-error'); // set error class to the control group
    },

    success: function(label) {
        label.closest('div').removeClass('has-error');
        label.remove();
    },

    errorElement: 'span',
    errorClass: 'help-block'
});

// Click button add new
$('#btnCatAddNew').click(function() {
    var isValid = $('#frmCatAdd').valid();
    if (isValid) {
        var CatName = $('#txtCatName').val();
        var poco = {
            CatName: CatName
        };

        $.ajax({
            url: 'http://localhost:6500/cat',
            dataType: 'json',
            contentType: 'application/json',
            type: 'POST',
            timeout: 10000,
            data: JSON.stringify(poco),
            headers: {
                'x-access-token': localStorage.access_token
            }
        }).done(function(data) {
            if (data.msg === "error level") {
                $('#LoginModal').modal('show');
                swal("Thông Báo", "Tài khoản không đủ quyền", "warning");
                $('#LoginModal').modal('show');
            }

            swal("Thông Báo", "Tạo thành công", "success");
            $('#CatAddModal').modal('toggle');

            console.log(data);

            var tr = '<tr>' +
                '<td>' + data.CatID + '</td>' +
                '<td class="catname">' + data.CatName + '</td>' +
                '<td>' +
                '<button style="margin-right:4px" type="button" class="btn btn-danger btnCatDel" data-id="' + data.CatID + '">' +
                    'Xóa <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>' +
                '</button>' +
                '<button type="button" class="btn btn-info btnCatEdit" data-id="' + data.CatID + '">' +
                    'Sửa <span class="glyphicon glyphicon-edit" aria-hidden="true"></span>' +
                '</button>' +
                '</td>' +
                '</tr>';
            $('#cat-list').prepend(tr);
        }).fail(function(xhr, textStatus, error) {
            console.log(xhr);
            console.log(textStatus);
            console.log(error);

            if (xhr.responseJSON.msg === 'verify fail') {
                $('#LoginModal').modal('show');
                swal("Thông Báo", "Token hết hạn, vui lòng đăng nhập lại.", "warning");
                $('#LoginModal').modal('show');
            } else if (xhr.responseJSON.msg === 'no token found') {
                $('#LoginModal').modal('show');
                swal("Thông Báo", "Không tìm thấy token, vui lòng đăng nhập.", "warning");
                $('#LoginModal').modal('show');
            } else {
                $('#LoginModal').modal('show');
            }
        });
    }
});

// Click button edit on Cat-List
$('#cat-list').on('click', '.btnCatEdit', function() {
    $('#CatEditModal').modal('show');

    var tr = $(this).closest('tr');
    var id = $(this).data('id');

    var CatName = tr.find('.catname').text();
    $('#txtCatNameEdit').val(CatName);
    $('#txtCatNameEdit').data('id', id);
});

// Validate form cat edit
$('#frmCatEdit').validate({
    rules: {
        txtCatNameEdit: {
            required: true
        }
    },
    messages: {
        txtCatNameEdit: {
            required: 'Nhập tên chuyên mục'
        }
    },

    highlight: function(element) { // hightlight error inputs
        $(element)
            .closest('div')
            .addClass('has-error'); // set error class to the control group
    },

    success: function(label) {
        label.closest('div').removeClass('has-error');
        label.remove();
    },

    errorElement: 'span',
    errorClass: 'help-block'
});

// Focus txtEmail if modal open
$('#CatEditModal').on('shown.bs.modal', function() {
    $('#txtCatNameEdit').focus();
});

// Click to update database
$('#btnCatAddEdit').click(function() {
    var isValid = $('#frmCatEdit').valid();
    if (isValid) {
        var CatName = $('#txtCatNameEdit').val();
        var CatID = $('#txtCatNameEdit').data('id');

        var poco = {
            CatName: CatName,
            CatID: CatID
        }

        $.ajax({
            url: 'http://localhost:6500/cat',
            type: 'PUT',
            contentType: 'application/json',
            dataType: 'json',
            timeout: 10000,
            data: JSON.stringify(poco),
            headers: {
                'x-access-token': localStorage.access_token
            }
        }).done(function(data) {
            console.log(data);
            if (data.msg === "error level") {
                $('#LoginModal').modal('show');
                swal("Thông Báo", "Tài khoản không đủ quyền", "warning");
                $('#LoginModal').modal('show');
            }

            if (data == 1) {
                swal("Thông Báo", "Sửa thành công.", "success");
                $('#CatEditModal').modal('toggle');

                var button = $('#cat-list').find('[data-id='+ CatID +']');
                var tr = button.closest('tr');
                var tdCN = tr.find('.catname');
                tdCN.text(CatName);
            } else {
                swal("Thông Báo", "Có lỗi xảy ra.", "warning");
            }
        }).fail(function(xhr, textStatus, error) {
            console.log(xhr);
            console.log(textStatus);
            console.log(error);

            if (xhr.responseJSON.msg === 'verify fail') {
                $('#LoginModal').modal('show');
                swal("Thông Báo", "Token hết hạn, vui lòng đăng nhập lại.", "warning");
                $('#LoginModal').modal('show');
            } else if (xhr.responseJSON.msg === 'no token found') {
                $('#LoginModal').modal('show');
                swal("Thông Báo", "Không tìm thấy token, vui lòng đăng nhập.", "warning");
                $('#LoginModal').modal('show');
            } else {
                $('#LoginModal').modal('show');
            }
        });
    }
});