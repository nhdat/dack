// Begin
$(function() {
    verifyToken();
    getCatList();
});

function verifyToken() {
    var UserID = localStorage.UserID;
    $.ajax({
        url: 'http://localhost:6500/user/' + UserID,
        dataType: 'json',
        timeout: 10000,
        type: 'GET',
        headers: {
            'x-access-token': localStorage.access_token
        }
    }).done(function(data) {
        $('#txtUserName').text(data.UserName);

        $('#liUserInfo').removeClass('hidden');
        $('#liLogout').removeClass('hidden');

        $('#liShowLogin').addClass('hidden');
        $('#liShowRegister').addClass('hidden');

        // add info to input
        $('#txtUserNameInfo').val(data.UserName);
        $('#txtUserEmailInfo').val(data.UserEmail);
        $('#txtUserAddInfo').val(data.UserAdd);

        if(data.UserType == 'sell'){
            $('#txtUserTypeInfo').val('Nguời bán');
        }else{
            $('#txtUserTypeInfo').val('Người mua');
        }
        
    }).catch(function(xhr, textStatus, error) {
        console.log(xhr.responseJSON.msg);
        console.log(textStatus);
        console.log(error);
    });
}

// Function save token to local storage
function saveToken(poco) {
    $.ajax({
        url: "http://localhost:6500/login",
        dataType: 'json',
        timeout: 10000,
        type: 'POST',
        contentType: 'application/json',
        data: JSON.stringify(poco)
    }).done(function(data) {
        console.log(data);
        // Cấp phát token thành công.
        localStorage.access_token = data.access_token;
        localStorage.UserID = data.UserID;
        localStorage.UserEmail = data.UserEmail;
        $('#LoginModal').modal('toggle');

        verifyToken();
    }).fail(function(xhr, textStatus, err) {
        console.log(xhr);
        console.log(textStatus);
        console.log(err);

        swal("Thông Báo", "Email hoặc mật khẩu không đúng.", "warning");
    });
}

// Get All Cat
function getCatList() {
    $.ajax({
        url: 'http://localhost:6500/cat',
        dataType: 'json',
        timeout: 10000
    }).done(function(data) {
        source = $('#CatListSearch-template').html();
        template = Handlebars.compile(source);
        html = template(data);
        $('#CatListSearch').append(html);
    }).catch(function(xhr, textStatus, error) {
        console.log(xhr);
        console.log(textStatus);
        console.log(error);
    });
}

///---------------------------------------------------------------------------------------------------///

/////////////////////////////////////// Event of User Info ////////////////////////////////////////

// Click button login
$('#btnUserInfo').click(function() {
    window.location.replace("http://localhost:8080/user");
});

// Validation form UserUpdate
$('#frmUserUpdate').validate({
    rules: {
        txtUserNameInfo: {
            required: true,
            minlength: 2
        },
        txtUserEmailInfo: {
            required: true,
            email: true
        },
        txtUserAddInfo: {
            required: true,
            minlength: 6
        }
    },
    messages: {
        txtUserNameInfo: {
            required: "Nhập tên",
            minlength: "Nhập ít nhất 2 ký tự"
        },
        txtUserEmailInfo: {
            required: "Nhập email",
            email: "Nhập email hợp lệ"
        },
        txtUserAddInfo: {
            required: "Nhập địa chỉ",
            minlength: "Nhập ít nhất 6 ký tự"
        }
    },

    highlight: function(element) { // hightlight error inputs
        $(element)
            .closest('div')
            .addClass('has-error'); // set error class to the control group
    },

    success: function(label) {
        label.closest('div').removeClass('has-error');
        label.remove();
    },

    errorElement: 'span',
    errorClass: 'help-block'
});

// Click button update user
$('#btnUserUpdate').click(function() {
    var isValid = $('#frmUserUpdate').valid();

    var poco = {

    }

    if(isValid){
        $('#PwdModal').modal('show');
    }
});

$('#btnPwdInfo').click(function(){
    var pwd = $('#txtPwdInfo').val();

    var poco = {
        UserID: localStorage.UserID,
        UserPwd : pwd
    }

    $.ajax({
        url: 'http://localhost:6500/user/pwd',
        type: 'POST',
        dataType: 'json',
        contentType: 'application/json',
        timeout: 10000,
        headers: {
            'x-access-token' : localStorage.access_token
        },
        data: JSON.stringify(poco)
    }).done(function(data){
        var email = $('#txtUserEmailInfo').val();
        if(email != localStorage.UserEmail){
            $.ajax({
                url: 'http://localhost:6500/user/email/' + email,
                type: 'GET',
                dataType: 'json',
                timeout: 10000
            }).done(function(data){
                if(data.length > 0){
                    swal("Thông báo!", "Email đã tồn tại!", "warning");
                }else{
                    swal("Thông báo!", "Cập nhật thành công!", "success");
                }
            }).catch(function(xhr, textStatus, error){
                console.log(xhr);
                console.log(textStatus);
                console.log(error);
            });
        }else{
            var poco = {
                UserName: $('#txtUserNameInfo').val(),
                UserEmail: $('#txtUserEmailInfo').val(),
                UserPwd: pwd,
                UserAdd: $('#txtUserAddInfo').val(),
                UserID: localStorage.UserID
            }

            $.ajax({
                url: 'http://localhost:6500/user',
                type: 'PUT',
                contentType: 'application/json',
                dataType: 'json',
                timeout: 10000,
                data: JSON.stringify(poco),
                headers: {
                    'x-access-token' : localStorage.access_token
                }
            }).done(function(data) {
                swal("Thông báo !", "Cập nhật thành công.", "success");
                
                $('#PwdModal').modal('toggle');
            }).fail(function(xhr, textStatus, error) {
                console.log(xhr);
                console.log(textStatus);
                console.log(error);

                swal("Thông báo !", "Cõ lối xảy ra.", "warning");
            });
        }
    }).fail(function(xhr, textStatus, error){
        console.log(xhr);
        console.log(textStatus);
        console.log(error);

        if (xhr.responseJSON === "password not correct") {
            swal("Thông báo !", "Mật khẩu không đúng.", "warning");
        } else {
            swal("Thông báo !", "Có lối xảy ra.", "warning");
            location.reload();
        }
    });
});


/////////////////////////////////////// End Event of User Info /////////////////////////////////////

///---------------------------------------------------------------------------------------------------///

/////////////////////////////////////// Event of Login & Logout ////////////////////////////////////////

// Click button login
$('#btnShowLogin').click(function() {
    $('#LoginModal').modal('show');
});

// If Login Modal open. Focus Email input.
$('#LoginModal').on('shown.bs.modal', function() {
    $('head').append('<style type="text/css">#LoginModal{overflow-y: scroll !important;}</style>');
    $('#txtEmail').focus();
});

// Validation form Login
$('#frmLogin').validate({
    rules: {
        email: {
            required: true,
            email: true
        },
        password: {
            required: true,
            minlength: 6
        }
    },
    messages: {
        email: {
            required: 'Nhập email',
            email: 'Nhập email đúng định dạng'
        },
        password: {
            required: "Chưa nhập mật khẩu.",
            minlength: "Mật khẩu phải nhiều hơn 6 ký tự."
        }
    },

    highlight: function(element) { // hightlight error inputs
        $(element)
            .closest('div')
            .addClass('has-error'); // set error class to the control group
    },

    success: function(label) {
        label.closest('div').removeClass('has-error');
        label.remove();
    },

    errorElement: 'span',
    errorClass: 'help-block'
});

// Click button login on modal login
$('#btnLogin').click(function() {
    var isValid = $('#frmLogin').valid();
    if (isValid) {
        var email = $('#txtEmail').val();
        var pwd = $('#txtPwd').val();

        var poco = {
            UserEmail: email,
            UserPwd: pwd
        }

        saveToken(poco);
    }
});

// Click button logout
$('#btnLogout').click(function() {
    localStorage.clear();
    location.reload();
});

/////////////////////////////////////// End Event of Login & Logout /////////////////////////////////////

///---------------------------------------------------------------------------------------------------///

/////////////////////////////////////// Event of Register ///////////////////////////////////////////////
// Click button register
$('#btnShowRegister').click(function() {
    $('#RegisterModal').modal('show');
});

// If Register Modal open. Focus Name input.
$('#RegisterModal').on('shown.bs.modal', function() {
    $('head').append('<style type="text/css">#RegisterModal{overflow-y: scroll !important;}</style>');
    $('#txtNameRes').focus();
});

// Validation form register
$('#frmRegister').validate({
    rules: {
        txtNameRes: {
            required: true,
            minlength: 2
        },
        txtEmailRes: {
            required: true,
            email: true
        },
        txtPwdRes: {
            required: true,
            minlength: 6
        },
        txtRepwdRes: {
            required: true,
            equalTo: "#txtPwdRes"
        },
        txtAddRes: {
            required: true,
            minlength: 2
        }
    },
    messages: {
        txtNameRes: {
            required: "Nhập họ tên của bạn.",
            minlength: "Nhập ít nhất 2 ký tự."
        },
        txtEmailRes: {
            required: "Nhập Email.",
            email: "Nhập Email hợp lệ."
        },
        txtPwdRes: {
            required: "Nhập mật khẩu.",
            minlength: "Nhập ít nhất 6 ký tự."
        },
        txtRepwdRes: {
            required: "Nhập lại mật khẩu",
            equalTo: "Nhập trùng mật khẩu."
        },
        txtAddRes: {
            required: "Nhập địa chỉ.",
            minlength: "Nhập ít nhất 2 ký tự."
        }
    },

    highlight: function(element) { // hightlight error inputs
        $(element)
            .closest('div')
            .addClass('has-error'); // set error class to the control group
    },

    success: function(label) {
        label.closest('div').removeClass('has-error');
        label.remove();
    },

    errorElement: 'span',
    errorClass: 'help-block'
});

// Click Button btnRegister
$('#btnRegister').click(function() {
    var isValid = $('#frmRegister').valid();
    if (isValid) {
        var captcha_res = grecaptcha.getResponse();
        if (captcha_res) {
            var poco = {
                captcha_res: captcha_res
            }

            $.ajax({
                url: 'http://localhost:6500/captcha',
                type: 'POST',
                dataType: 'json',
                contentType: 'application/json',
                timeout: 10000,
                data: JSON.stringify(poco)
            }).done(function(data) {
                if (data.success == true) {
                    var poco = {
                        UserName: $('#txtNameRes').val(),
                        UserEmail: $('#txtEmailRes').val(),
                        UserPwd: $('#txtPwdRes').val(),
                        UserAdd: $('#txtAddRes').val()
                    }

                    $.ajax({
                        url: 'http://localhost:6500/user',
                        type: 'POST',
                        contentType: 'application/json',
                        dataType: 'json',
                        timeout: 10000,
                        data: JSON.stringify(poco)
                    }).done(function(data) {
                        swal("Thông báo !", "Đăng ký thành công.", "success");
                        $('#RegisterModal').modal('toggle');
                    }).fail(function(xhr, textStatus, error) {
                        console.log(xhr);
                        console.log(textStatus);
                        console.log(error);

                        if (xhr.responseJSON.msg === "not unique email") {
                            swal("Thông báo !", "Email đã tồn tại.", "warning");
                            grecaptcha.reset();
                        } else {
                            swal("Thông báo !", "Cõ lối xảy ra.", "warning");
                            grecaptcha.reset();
                        }
                    });
                } else {
                    swal("Thông báo !", "Click vào captcha chưa hợp lệ.", "warning");
                    grecaptcha.reset();
                }
            }).catch(function(xhr, textStatus, error) {
                console.log(xhr);
                console.log(textStatus);
                console.log(error);

                swal("Thông báo !", "Có lỗi xảy ra.", "warning");
                grecaptcha.reset();
            });
        } else {
            swal("Thông báo!", "Có lỗi xảy ra.", "warning");
        }
    }
});

/////////////////////////////////////// End Event of Register ///////////////////////////////////////////////

///---------------------------------------------------------------------------------------------------///

///---------------------------------------------------------------------------------------------------///

///====================================  SEARCH PAGE   ===============================================================///

// Event Search /////////////////////////////////////////////////////////////////////

$('#btnSearch').click(function(){
    var txtSearch = $('#txtSearch').val();
    var sltCatList = $('#CatListSearch').val();
    $('#ProductListSearch').empty();

    if(txtSearch == ''){
        swal("Thông báo!", "Nhập thông tin tìm kiếm!", "warning");
    }else{
        $('#SearchModal').modal('show');

        pageCat = 1;
        pageQuery = 1;
        search(txtSearch, sltCatList);
    }
});

$('#LoadMoreSearchWithCat').click(function(){
    var poco = {
        query: txtSearch,
        cat: sltCatList,
        page: pageCat
    }
    loadSearchWithCat(poco);
});

$('#LoadMoreSearchNotCat').click(function(){
    var poco = {
        query: txtSearch,
        page: pageQuery
    }
    loadSearchNotCat(poco);
});

function search(txtSearch, sltCatList){
    $('#LoadMoreSearchWithCat').hide();
    $('#LoadMoreSearchNoCat').hide();

    if(sltCatList > 0){
        var poco = {
            query: txtSearch,
            cat: sltCatList,
            page: pageCat
        }
        loadSearchWithCat(poco);
    }else if(txtSearch){
        var poco = {
            query: txtSearch,
            page: pageQuery
        }

        loadSearchNotCat(poco);
    }
}

function loadSearchWithCat(poco){
    $('#loaderSearch').show();
    $.ajax({
        url: 'http://localhost:6500/product/searchwithcat',
        dataType: 'json',
        type: 'POST',
        contentType: 'application/json',
        timeout: 10000,
        data: JSON.stringify(poco)
    }).done(function(data){
        $('#loaderSearch').show();
        var source = $('#Search-template').html();
        var template = Handlebars.compile(source);
        var html = template(data.rows);
        
        $('#ProductListSearch').append(html);

        pageCat++;
        if (data.hasMore == "false") {
            $('#LoadMoreSearchWithCat').hide();
        } else {
            $('#LoadMoreSearchWithCat').show();
        }

        $('#loaderSearch').hide();
    }).fail(function(xhr, textStatus, error){
        console.log(xhr);
        console.log(textStatus);
        console.log(error);
    });
}

function loadSearchNotCat(poco){
    $('#loaderSearch').show();
    $.ajax({
        url: 'http://localhost:6500/product/searchnotcat',
        dataType: 'json',
        type: 'POST',
        contentType: 'application/json',
        timeout: 10000,
        data: JSON.stringify(poco)
    }).done(function(data){
        $('#loaderSearch').show();
        var source = $('#Search-template').html();
        var template = Handlebars.compile(source);
        var html = template(data.rows);
        
        $('#ProductListSearch').append(html);

        pageQuery++;
        if (data.hasMore == "false") {
            $('#LoadMoreSearchNotCat').hide();
        } else {
            $('#LoadMoreSearchNotCat').show();
        }

        $('#loaderSearch').hide();
    }).fail(function(xhr, textStatus, error){
        console.log(xhr);
        console.log(textStatus);
        console.log(error);
    });
}