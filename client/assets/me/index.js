// Begin
$(function() {
    verifyToken();
    getCatList();
});

function verifyToken() {
    var UserID = localStorage.UserID;
    $.ajax({
        url: 'http://localhost:6500/user/' + UserID,
        dataType: 'json',
        timeout: 10000, type: 'GET',
        headers: {
            'x-access-token': localStorage.access_token
        }
    }).done(function(data) {
        $('#txtUserName').text(data.UserName);

        $('#liUserInfo').removeClass('hidden');
        $('#liLogout').removeClass('hidden');

        $('#liShowLogin').addClass('hidden');
        $('#liShowRegister').addClass('hidden');

        $('#wellLevel').removeClass('hidden');
        console.log(data);
        if (data.UserType == 'sell') {
            $('#TypeSeller').removeClass('hidden');
            $('#btnRequestSeller').attr("disabled", true);
        }
    }).catch(function(xhr, textStatus, error) {
        console.log(xhr.responseJSON.msg);
        console.log(textStatus);
        console.log(error);
    });
}

// Function save token to local storage
function saveToken(poco) {
    $.ajax({
        url: "http://localhost:6500/login",
        dataType: 'json',
        timeout: 10000,
        type: 'POST',
        contentType: 'application/json',
        data: JSON.stringify(poco)
    }).done(function(data) {
        // Cấp phát token thành công.
        localStorage.access_token = data.access_token;
        localStorage.UserID = data.UserID;
        $('#LoginModal').modal('toggle');

        verifyToken();
    }).fail(function(xhr, textStatus, err) {
        console.log(xhr);
        console.log(textStatus);
        console.log(err);

        swal("Thông Báo", "Email hoặc mật khẩu không đúng.", "warning");
    });
}

// Get All Cat
function getCatList() {
    $.ajax({
        url: 'http://localhost:6500/cat',
        dataType: 'json',
        timeout: 10000
    }).done(function(data) {
        var source = $('#CatList-template').html();
        var template = Handlebars.compile(source);
        var html = template(data);
        $('#CatList').append(html);

        source = $('#CatListSearch-template').html();
        template = Handlebars.compile(source);
        html = template(data);
        $('#CatListSearch').append(html);

        source = $('#CatListCre-template').html();
        template = Handlebars.compile(source);
        html = template(data);
        $('#CatListCre').append(html);
    }).catch(function(xhr, textStatus, error) {
        console.log(xhr);
        console.log(textStatus);
        console.log(error);
    });
}

// Element has class money will be formated currency number
$('.money').simpleMoneyFormat();

///---------------------------------------------------------------------------------------------------///

/////////////////////////////////////// Event of User Info ////////////////////////////////////////

// Click button login
$('#btnUserInfo').click(function() {
    window.location.replace("http://localhost:8080/user");
});

/////////////////////////////////////// End Event of User Info /////////////////////////////////////

///---------------------------------------------------------------------------------------------------///

/////////////////////////////////////// Event of Login & Logout ////////////////////////////////////////

// Click button login
$('#btnShowLogin').click(function() {
    $('#LoginModal').modal('show');
});

// If Login Modal open. Focus Email input.
$('#LoginModal').on('shown.bs.modal', function() {
    $('head').append('<style type="text/css">#LoginModal{overflow-y: scroll !important;}</style>');
    $('#txtEmail').focus();
});

// Validation form Login
$('#frmLogin').validate({
    rules: {
        email: {
            required: true,
            email: true
        },
        password: {
            required: true,
            minlength: 6
        }
    },
    messages: {
        email: {
            required: 'Nhập email',
            email: 'Nhập email đúng định dạng'
        },
        password: {
            required: "Chưa nhập mật khẩu.",
            minlength: "Mật khẩu phải nhiều hơn 6 ký tự."
        }
    },

    highlight: function(element) { // hightlight error inputs
        $(element)
            .closest('div')
            .addClass('has-error'); // set error class to the control group
    },

    success: function(label) {
        label.closest('div').removeClass('has-error');
        label.remove();
    },

    errorElement: 'span',
    errorClass: 'help-block'
});

// Click button login on modal login
$('#btnLogin').click(function() {
    var isValid = $('#frmLogin').valid();
    if (isValid) {
        var email = $('#txtEmail').val();
        var pwd = $('#txtPwd').val();

        var poco = {
            UserEmail: email,
            UserPwd: pwd
        }

        saveToken(poco);
    }
});

// Click button logout
$('#btnLogout').click(function() {
    localStorage.clear();
    location.reload();
});

/////////////////////////////////////// End Event of Login & Logout /////////////////////////////////////

///---------------------------------------------------------------------------------------------------///

/////////////////////////////////////// Event of Register ///////////////////////////////////////////////
// Click button register
$('#btnShowRegister').click(function() {
    $('#RegisterModal').modal('show');
});

// If Register Modal open. Focus Name input.
$('#RegisterModal').on('shown.bs.modal', function() {
    $('head').append('<style type="text/css">#RegisterModal{overflow-y: scroll !important;}</style>');
    $('#txtNameRes').focus();
});

// Validation form register
$('#frmRegister').validate({
    rules: {
        txtNameRes: {
            required: true,
            minlength: 2
        },
        txtEmailRes: {
            required: true,
            email: true
        },
        txtPwdRes: {
            required: true,
            minlength: 6
        },
        txtRepwdRes: {
            required: true,
            equalTo: "#txtPwdRes"
        },
        txtAddRes: {
            required: true,
            minlength: 2
        }
    },
    messages: {
        txtNameRes: {
            required: "Nhập họ tên của bạn.",
            minlength: "Nhập ít nhất 2 ký tự."
        },
        txtEmailRes: {
            required: "Nhập Email.",
            email: "Nhập Email hợp lệ."
        },
        txtPwdRes: {
            required: "Nhập mật khẩu.",
            minlength: "Nhập ít nhất 6 ký tự."
        },
        txtRepwdRes: {
            required: "Nhập lại mật khẩu",
            equalTo: "Nhập trùng mật khẩu."
        },
        txtAddRes: {
            required: "Nhập địa chỉ.",
            minlength: "Nhập ít nhất 2 ký tự."
        }
    },

    highlight: function(element) { // hightlight error inputs
        $(element)
            .closest('div')
            .addClass('has-error'); // set error class to the control group
    },

    success: function(label) {
        label.closest('div').removeClass('has-error');
        label.remove();
    },

    errorElement: 'span',
    errorClass: 'help-block'
});

// Click Button btnRegister
$('#btnRegister').click(function() {
    var isValid = $('#frmRegister').valid();
    if (isValid) {
        var captcha_res = grecaptcha.getResponse();
        if (captcha_res) {
            var poco = {
                captcha_res: captcha_res
            }

            $.ajax({
                url: 'http://localhost:6500/captcha',
                type: 'POST',
                dataType: 'json',
                contentType: 'application/json',
                timeout: 10000,
                data: JSON.stringify(poco)
            }).done(function(data) {
                if (data.success == true) {
                    var poco = {
                        UserName: $('#txtNameRes').val(),
                        UserEmail: $('#txtEmailRes').val(),
                        UserPwd: $('#txtPwdRes').val(),
                        UserAdd: $('#txtAddRes').val()
                    }

                    $.ajax({
                        url: 'http://localhost:6500/user',
                        type: 'POST',
                        contentType: 'application/json',
                        dataType: 'json',
                        timeout: 10000,
                        data: JSON.stringify(poco)
                    }).done(function(data) {
                        swal("Thông báo !", "Đăng ký thành công.", "success");
                        $('#RegisterModal').modal('toggle');
                    }).fail(function(xhr, textStatus, error) {
                        console.log(xhr);
                        console.log(textStatus);
                        console.log(error);

                        if (xhr.responseJSON.msg === "not unique email") {
                            swal("Thông báo !", "Email đã tồn tại.", "warning");
                            grecaptcha.reset();
                        } else {
                            swal("Thông báo !", "Cõ lối xảy ra.", "warning");
                            grecaptcha.reset();
                        }
                    });
                } else {
                    swal("Thông báo !", "Click vào captcha chưa hợp lệ.", "warning");
                    grecaptcha.reset();
                }
            }).catch(function(xhr, textStatus, error) {
                console.log(xhr);
                console.log(textStatus);
                console.log(error);

                swal("Thông báo !", "Có lỗi xảy ra.", "warning");
                grecaptcha.reset();
            });
        } else {
            swal("Thông báo!", "Có lỗi xảy ra.", "warning");
        }
    }
});

/////////////////////////////////////// End Event of Register ///////////////////////////////////////////////

///---------------------------------------------------------------------------------------------------///








////////////////////////////////////////////// BUYER ///////////////////////////////////////////////////////

// Click button Request Seller
$('#btnRequestSeller').click(function(){
    $.ajax({
        url: 'http://localhost:6500/user/requestseller',
        dataType: 'json',
        type: 'POST',
        timeout: 10000,
        headers: {
            'x-access-token' : localStorage.access_token
        }
    }).done(function(data){
        if(data == 1){
            $('#btnRequestSeller').attr('disabled', true);
            swal("Thông báo !", "Xin thành công. Hãy đợi duyệt", "success");
        }
    }).catch(function(xhr, textStatus, error){
        console.log(xhr);
        console.log(textStatus);
        console.log(error);
    });
});

///---------------------------------------------------------------------------------------------------///

// Event of Show Watch List ///////////////////////////////////////////////

var pagePr = 1;
// Click button show product list
$('#btnShowWatchList').click(function(){
    $('#WatchListModal').modal('show');

    $('#WatchList').empty();
    pagePr = 1;
    loadProductWatchList();
});

// Function add row to Product List
function loadProductWatchList(){
    $('#loaderWatchList').show();
    $.ajax({
        url: 'http://localhost:6500/product/watchlist/' + pagePr,
        dataType: 'json',
        timeout: 10000,
        headers: {
            'x-access-token' : localStorage.access_token
        }
    }).done(function(data){
        console.log(data);

        var rows = data.rows;
        for(var i = 0; i < rows.length; i++){
            if(rows[i].PrTimeStart)
                rows[i].PrTimeStart = formatDateTime(rows[i].PrTimeStart);
            if(rows[i].PrTimeEnd)
                rows[i].PrTimeEnd = formatDateTime(rows[i].PrTimeEnd);
        }

        var source = $('#WatchList-template').html();
        var template = Handlebars.compile(source);
        var html = template(data.rows);
        $('#WatchList').append(html);

        $('#WatchList tr[style]').fadeIn('slow', function(){
            $(this).removeAttr('style');
        });

        if(data.hasMore == 'false'){
            $('#LoadMoreWatchList').hide();
        }else{
            $('#LoadMoreWatchList').show();
        }

        pagePr++;

        $('#loaderWatchList').hide();

    }).fail(function(xhr, textStatus, error){
        console.log(xhr);
        console.log(textStatus);
        console.log(error);

        $('#ProductListNotEndModal').modal('toggle');

        if (xhr.responseJSON.msg === 'verify fail') {
            $('#LoginModal').modal('show');
            swal("Thông Báo", "Token hết hạn, vui lòng đăng nhập lại.", "warning");
            $('#LoginModal').modal('show');
        } else if (xhr.responseJSON.msg === 'no token found') {
            $('#LoginModal').modal('show');
            swal("Thông Báo", "Không tìm thấy token, vui lòng đăng nhập.", "warning");
            $('#LoginModal').modal('show');
        } else {
            swal("Thông Báo", "Có lỗi xảy ra.", "warning");
        }
    });
}

// Load More Product List Not End
$('#LoadMoreWatchList').click(function(){
    loadProductWatchList();
});

// Click button Product List View
$('#WatchList').on('click', '#btnProductListView', function(){
    var id = $(this).data('id');
    window.location.replace("http://localhost:8080/product?id=" + id);
});

// Click button Product List Delete
$('#WatchList').on('click', '#btnProductListDel', function(){
    var tr = $(this).closest('tr');

    var id = $(this).data('id');
    var UserID = localStorage.UserID;
    
    swal("Bạn có muốn xóa khỏi danh sách?", {
      buttons: ["Cancel!", true],
    }).then(function(willdelete){
        if(willdelete){
            $.ajax({
                url: 'http://localhost:6500/watchlist/' + UserID + "/" + id,
                type: 'DELETE',
                timeout: 10000,
                dataType: 'json'
            }).done(function(data){
                //console.log(data);
                if(data == 1){
                    tr.fadeOut('slow', function(){
                        swal("Thông báo!", "Xóa thành công!", "success");
                        tr.remove();
                    });
                }
            }).fail(function(xhr, textStatus, error){
                console.log(xhr);
                console.log(textStatus);
                console.log(error);
            });
        }
    });
});

// End Event of Show Watch List ///////////////////////////////////////////////

///---------------------------------------------------------------------------------------------------///

///---------------------------------------------------------------------------------------------------///

// Event of Show Bidded List ///////////////////////////////////////////////

var pagePr = 1;
// Click button show product list
$('#btnShowBiddedList').click(function(){
    $('#BiddedModal').modal('show');

    $('#Bidded').empty();
    pagePr = 1;
    loadBiddedList();
});

// Function add row to Product List
function loadBiddedList(){
    $('#loaderBidded').show();
    $.ajax({
        url: 'http://localhost:6500/product/bidded/' + pagePr,
        dataType: 'json',
        timeout: 10000,
        headers: {
            'x-access-token' : localStorage.access_token
        }
    }).done(function(data){
        console.log(data);

        var rows = data.rows;
        for(var i = 0; i < rows.length; i++){
            if(rows[i].PrTimeStart)
                rows[i].PrTimeStart = formatDateTime(rows[i].PrTimeStart);
            if(rows[i].PrTimeEnd)
                rows[i].PrTimeEnd = formatDateTime(rows[i].PrTimeEnd);
        }

        var source = $('#ProductListNotEdit-template').html();
        var template = Handlebars.compile(source);
        var html = template(data.rows);
        $('#Bidded').append(html);

        $('#Bidded tr[style]').fadeIn('slow', function(){
            $(this).removeAttr('style');
        });

        if(data.hasMore == 'false'){
            $('#LoadMoreBidded').hide();
        }else{
            $('#LoadMoreBidded').show();
        }

        pagePr++;

        $('#loaderBidded').hide();

    }).fail(function(xhr, textStatus, error){
        console.log(xhr);
        console.log(textStatus);
        console.log(error);
        swal("Thông Báo", "Có lỗi xảy ra.", "warning");
    });
}

// Load More Product List Not End
$('#LoadMoreBidded').click(function(){
    loadBiddedList();
});

// Click button Product List View
$('#Bidded').on('click', '#btnProductListView', function(){
    var id = $(this).data('id');
    window.location.replace("http://localhost:8080/product?id=" + id);
});

// End Event of Bidded List ///////////////////////////////////////////////

///---------------------------------------------------------------------------------------------------///

// Event of Show Bidding List ///////////////////////////////////////////////

var pagePr = 1;
// Click button show product list
$('#btnShowBiddingList').click(function(){
    $('#BiddingModal').modal('show');

    $('#Bidding').empty();
    pagePr = 1;
    loadBiddingList();
});

// Function add row to Product List
function loadBiddingList(){
    $('#loaderBidding').show();
    $.ajax({
        url: 'http://localhost:6500/product/bidding/' + pagePr,
        dataType: 'json',
        timeout: 10000,
        headers: {
            'x-access-token' : localStorage.access_token
        }
    }).done(function(data){
        console.log(data);

        var rows = data.rows;
        for(var i = 0; i < rows.length; i++){
            if(rows[i].PrTimeStart)
                rows[i].PrTimeStart = formatDateTime(rows[i].PrTimeStart);
            if(rows[i].PrTimeEnd)
                rows[i].PrTimeEnd = formatDateTime(rows[i].PrTimeEnd);
        }

        var source = $('#ProductListNotEdit-template').html();
        var template = Handlebars.compile(source);
        var html = template(data.rows);
        $('#Bidding').append(html);

        $('#Bidding tr[style]').fadeIn('slow', function(){
            $(this).removeAttr('style');
        });

        if(data.hasMore == 'false'){
            $('#LoadMoreBidding').hide();
        }else{
            $('#LoadMoreBidding').show();
        }

        pagePr++;

        $('#loaderBidding').hide();

    }).fail(function(xhr, textStatus, error){
        console.log(xhr);
        console.log(textStatus);
        console.log(error);
        swal("Thông Báo", "Có lỗi xảy ra.", "warning");
    });
}

// Load More Product List Not End
$('#LoadMoreBidding').click(function(){
    loadBiddedList();
});

// Click button Product List View
$('#Bidding').on('click', '#btnProductListView', function(){
    var id = $(this).data('id');
    window.location.replace("http://localhost:8080/product?id=" + id);
});

// End Event of Bidded List ///////////////////////////////////////////////

///---------------------------------------------------------------------------------------------------///










/////////////////////////////////////////////////////// SELLER ///////////////////////////////////////////////////////

// Event of CreateProduct ///////////////////////////////////////////////
// Repalce ckeditor
CKEDITOR.replace('txtConentCre');

// Set min day for TimeStart
var TimeStart = new Date();
TimeStart.setMinutes(TimeStart.getMinutes() + 1);
$('#txtTimeStart').datetimepicker({
    sideBySide: true,
    minDate: TimeStart
});

var TimeEnd = new Date();
TimeEnd.setMinutes(TimeEnd.getMinutes() + 2);
$('#txtTimeEnd').datetimepicker({
    sideBySide: true,
    minDate: TimeEnd
});

// Show preview image if upload image of event create product
$("#imgArrCre").on('change', function() {
    //Get count of selected files
    var countFiles = $(this)[0].files.length;
    if (countFiles > 3) {
        swal("Thông Báo", "Chỉ được đăng 3 tấm hình về sản phẩm.", "warning");
        $("#imgArrCre").val('');
        $('#image-holder').empty();
    } else {
        var imgPath = $(this)[0].value;
        var extn = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
        var image_holder = $("#image-holder");
        image_holder.empty();

        if (extn == "gif" || extn == "png" || extn == "jpg" || extn == "jpeg") {
            if (typeof(FileReader) != "undefined") {

                //loop for each file selected for uploaded.
                for (var i = 0; i < countFiles; i++) {

                    var reader = new FileReader();
                    reader.onload = function(e) {
                        $("<img />", {
                            "src": e.target.result,
                            "class": "img-rounded img-cre"
                        }).appendTo(image_holder);
                    }

                    image_holder.show();
                    reader.readAsDataURL($(this)[0].files[i]);
                }

            } else {
                swal("Thông Báo", "Có lỗi xảy ra.", "warning");
            }
        } else {
            swal("Thông Báo", "Chỉ upload hình ảnh.", "warning");
        }
    }
});

// Click button Create Product
$('#btnShowCreateProduct').click(function() {
    $('#CreateProductModal').modal('show');
});

// If Create Product Modal open. Add style to header.
$('#CreateProductModal').on('shown.bs.modal', function() {
    $('head').append('<style type="text/css">#CreateProductModal{overflow-y: scroll !important;}</style>');
    $('#CatListCre').focus();
});

// Add method not euqal value to validation jquery
$.validator.addMethod("valueNotEquals", function(value, element, arg){
    return arg !== value;
}, "Value must not equal arg.");

// Add method Check DateTime to validation jquery
$.validator.addMethod("datetimeNotCorrect", function(value, element, arg){
    var timestart = $(arg).val();
    var timeend = value;

    return timeend > timestart;
}, "Value must > arg.");

// Validation form create product
$('#frmCreateProduct').validate({
    ignore: [],
    rules: {
        sltCatCre: {
            valueNotEquals: "0"
        },
        txtNameCre: {
            required: true
        },
        imgArrCre: {
            required: true
        },
        txtPriceOfferCre: {
            required: true,
            number: true
        },
        txtPriceStepCre: {
            required: true,
            number: true
        },
        txtPriceBuyNowCre: {
            number: true
        },
        txtTimeStart: {
            required: true
        },
        txtTimeEnd: {
            required: true,
            datetimeNotCorrect: '#txtTimeStart'
        },
        txtConentCre: {
            required: function(){CKEDITOR.instances.txtConentCre.updateElement();},
            minlength: 10
        }
    },
    messages: {
        sltCatCre: {
            valueNotEquals: "Chọn chuyên mục."
        },
        txtNameCre: {
            required: "Nhập tên sản phẩm."
        },
        imgArrCre: {
            required: "Chọn ít nhất 1 tấm ảnh."
        },
        txtPriceOfferCre: {
            required: "Nhập giá khởi điểm",
            number: "Nhập số tiền."
        },
        txtPriceStepCre: {
            required: "Nhập bước giá.",
            number: "Nhập số tiền."
        },
        txtPriceBuyNowCre: {
            number: "Nhập số tiền."
        },
        txtTimeStart: {
            required: "Nhập thời gian bắt đầu."
        },
        txtTimeEnd: {
            required: "Nhập thời gian kết thúc.",
            datetimeNotCorrect: "Thời gian kết thúc không hợp lệ."
        },
        txtConentCre: {
            required: "Nhập mô tả",
            minlength: "Nhập ít nhất 10 ký tự"
        }
    },

    highlight: function(element) { // hightlight error inputs
        $(element)
            .closest('div')
            .addClass('has-error'); // set error class to the control group
    },

    success: function(label) {
        label.closest('div').removeClass('has-error');
        label.remove();
    },

    errorElement: 'span',
    errorClass: 'help-block'
});

// Ajax submit form Create Product
$('#frmCreateProduct').submit(function(){
    $(this).ajaxSubmit(options);

    return false;
});

var options = {
    beforeSubmit: showRequest,
    success: showResponse,
    url: 'http://localhost:6500/product',
    type: 'POST',
    headers: {
        'x-access-token' : localStorage.access_token
    }
}

function showRequest(formData, jqForm, options) {
    var isValid = $('#frmCreateProduct').valid();
    if(isValid){
        swal("Thông báo!", "Đang upload sản phẩm", "info");
    }else{
        return false;
    }
}

function showResponse(responseText, statusText, xhr, $form) {
    console.log(responseText);
    console.log(statusText);
    console.log(xhr);

    if(statusText == 'success'){
        swal("Thông báo!", "Tạo sản phẩm thành công.", "success");
        $('#CreateProductModal').modal('toggle');
    }
}

// End Event of CreateProduct ///////////////////////////////////////////////

///---------------------------------------------------------------------------------------------------///

// Event of Show Product List ///////////////////////////////////////////////
function formatDateTime(datetime){
    var split = datetime.split(' ');
    var date = split[0];
    var time = split[1];

    //console.log(date);
    var splitdate = date.split('-');
    var year = splitdate[0];
    var month = splitdate[1];
    var day = splitdate[2];

    //console.log(time);
    splittime = time.split(':');
    var hours = splittime[0];
    var minute = splittime[1];
    var second = splittime[2];

    var str = day + "/" + month + "/" + year + " (" + hours + ":" + minute + ":" + second +")";
    return str;
}

var pagePr = 1;
// Click button show product list
$('#btnShowProductList').click(function(){
    $('#ProductListModal').modal('show');

    $('#ProductList').empty();
    pagePr = 1;
    loadProductList();
});

// Function add row to Product List
function loadProductList(){
    $('#loader').show();
    $.ajax({
        url: 'http://localhost:6500/product/post/' + pagePr,
        dataType: 'json',
        timeout: 10000,
        headers: {
            'x-access-token' : localStorage.access_token
        }
    }).done(function(data){
        console.log(data);
        var rows = data.rows;
        for(var i = 0; i < rows.length; i++){
            if(rows[i].PrTimeStart)
                rows[i].PrTimeStart = formatDateTime(rows[i].PrTimeStart);
            if(rows[i].PrTimeEnd)
                rows[i].PrTimeEnd = formatDateTime(rows[i].PrTimeEnd);
        }

        var source = $('#ProductList-template').html();
        var template = Handlebars.compile(source);
        var html = template(data.rows);
        $('#ProductList').append(html);

        $('#ProductList tr[style]').fadeIn('slow', function(){
            $(this).removeAttr('style');
        });

        if(data.hasMore == 'false'){
            $('#LoadMoreProduct').hide();
        }else{
            $('#LoadMoreProduct').show();
        }

        pagePr++;

        $('#loader').hide();

    }).fail(function(xhr, textStatus, error){
        console.log(xhr);
        console.log(textStatus);
        console.log(error);

        $('#ProductListModal').modal('toggle');

        if (xhr.responseJSON.msg === 'verify fail') {
            $('#LoginModal').modal('show');
            swal("Thông Báo", "Token hết hạn, vui lòng đăng nhập lại.", "warning");
            $('#LoginModal').modal('show');
        } else if (xhr.responseJSON.msg === 'no token found') {
            $('#LoginModal').modal('show');
            swal("Thông Báo", "Không tìm thấy token, vui lòng đăng nhập.", "warning");
            $('#LoginModal').modal('show');
        } else {
            swal("Thông Báo", "Có lỗi xảy ra.", "warning");
        }
    });
}

// Load More Product
$('#LoadMoreProduct').click(function(){
    loadProductList();
});

// Click button Product List View
$('#ProductList').on('click', '#btnProductListView', function(){
    var id = $(this).data('id');
    window.location.replace("http://localhost:8080/product?id=" + id);
});

// Click button Product List Update
// Repalce ckeditor
var ContentUpdate = CKEDITOR.replace('txtConentUpdate');
var ContentOld = CKEDITOR.replace('txtConentOld');
$('#ProductList').on('click', '#btnProductListUpdate', function(){
    $('#UpdateProductContentModal').modal('show');

    var id = $(this).data('id');

    $.ajax({
        url: 'http://localhost:6500/product/' + id,
        dataType: 'json',
        timeout: 10000
    }).done(function(data){
        console.log(data);

        $('#txtNameUpdate').val(data.PrName);
        //$('#txtConentOld').val(data.PrContent);
        ContentOld.setData(data.PrContent);

        $('#btnUpdateContent').data('id', data.PrID);

    }).fail(function(xhr, textStatus, error){
        console.log(xhr);
        console.log(textStatus);
        console.log(error);

        swal("Thông Báo", "Có lỗi xảy ra.", "warning");
    });
});

// If Product Update Modal open. Add style to header.
$('#UpdateProductContentModal').on('shown.bs.modal', function() {
    $('head').append('<style type="text/css">#UpdateProductContentModal{overflow-y: scroll !important;}</style>');
    $('#txtConentUpdate').focus();
});

$('#btnUpdateContent').click(function(){
    var content = ContentUpdate.getData();
    if(content == ''){
        swal("Thông Báo", "Nhập nội dung mới cần cập nhật.", "error");
    }else{
        var id = $('#btnUpdateContent').data('id');
        var contentOld = ContentOld.getData();

        var poco = {
            PrContentOld: contentOld,
            PrContentNew: content
        }

        $.ajax({
            url: 'http://localhost:6500/product/content/' + id,
            type: 'PUT',
            dataType: 'json',
            contentType: 'application/json',
            timeout: 10000,
            data: JSON.stringify(poco)
        }).done(function(data){
            if(data == 1){
                swal("Thông Báo", "Cập nhật thành công.", "success");
                $('#UpdateProductContentModal').modal('toggle');
                ContentUpdate.setData('');
            }else{
                swal("Thông Báo", "Cập nhật thất bại.", "warning");
            }
        }).fail(function(xhr, textStatus, error){
            console.log(xhr);
            console.log(textStatus);
            console.log(error);
        });
    }
});

// End Event of Show Product List ///////////////////////////////////////////////

///---------------------------------------------------------------------------------------------------///

// Event of Show Product List Not End ///////////////////////////////////////////////

var pagePr = 1;
// Click button show product list
$('#btnShowProductListNotEnd').click(function(){
    $('#ProductListNotEndModal').modal('show');

    $('#ProductListNotEnd').empty();
    pagePr = 1;
    loadProductListNotEnd();
});

// Function add row to Product List
function loadProductListNotEnd(){
    $('#loaderNotEnd').show();
    $.ajax({
        url: 'http://localhost:6500/product/notend/' + pagePr,
        dataType: 'json',
        timeout: 10000,
        headers: {
            'x-access-token' : localStorage.access_token
        }
    }).done(function(data){
        console.log(data);

        var rows = data.rows;
        for(var i = 0; i < rows.length; i++){
            if(rows[i].PrTimeStart)
                rows[i].PrTimeStart = formatDateTime(rows[i].PrTimeStart);
            if(rows[i].PrTimeEnd)
                rows[i].PrTimeEnd = formatDateTime(rows[i].PrTimeEnd);
        }

        var source = $('#ProductListNotEdit-template').html();
        var template = Handlebars.compile(source);
        var html = template(data.rows);
        $('#ProductListNotEnd').append(html);

        $('#ProductListNotEnd tr[style]').fadeIn('slow', function(){
            $(this).removeAttr('style');
        });

        if(data.hasMore == 'false'){
            $('#LoadMoreProductListNotEnd').hide();
        }else{
            $('#LoadMoreProductListNotEnd').show();
        }

        pagePr++;

        $('#loaderNotEnd').hide();

    }).fail(function(xhr, textStatus, error){
        console.log(xhr);
        console.log(textStatus);
        console.log(error);

        $('#ProductListNotEndModal').modal('toggle');

        if (xhr.responseJSON.msg === 'verify fail') {
            $('#LoginModal').modal('show');
            swal("Thông Báo", "Token hết hạn, vui lòng đăng nhập lại.", "warning");
            $('#LoginModal').modal('show');
        } else if (xhr.responseJSON.msg === 'no token found') {
            $('#LoginModal').modal('show');
            swal("Thông Báo", "Không tìm thấy token, vui lòng đăng nhập.", "warning");
            $('#LoginModal').modal('show');
        } else {
            swal("Thông Báo", "Có lỗi xảy ra.", "warning");
        }
    });
}

// Load More Product List Not End
$('#LoadMoreProductListNotEnd').click(function(){
    loadProductListNotEnd();
});

// Click button Product List Not End View
$('#ProductListNotEnd').on('click', '#btnProductListView', function(){
    var id = $(this).data('id');
    window.location.replace("http://localhost:8080/product?id=" + id);
});

// End Event of Show Product List Not End ///////////////////////////////////////////////

///---------------------------------------------------------------------------------------------------///

// Event of Show Product List Sold ///////////////////////////////////////////////

var pagePr = 1;
// Click button show product Sold list 
$('#btnShowProductListSold').click(function(){
    $('#ProductListSoldModal').modal('show');

    $('#ProductListSold').empty();
    pagePr = 1;
    loadProductListSold();
});

// Function add row to Product List
function loadProductListSold(){
    $('#loaderSold').show();
    $.ajax({
        url: 'http://localhost:6500/product/sold/' + pagePr,
        dataType: 'json',
        timeout: 10000,
        headers: {
            'x-access-token' : localStorage.access_token
        }
    }).done(function(data){
        console.log(data);

        var rows = data.rows;
        for(var i = 0; i < rows.length; i++){
            if(rows[i].PrTimeStart)
                rows[i].PrTimeStart = formatDateTime(rows[i].PrTimeStart);
            if(rows[i].PrTimeEnd)
                rows[i].PrTimeEnd = formatDateTime(rows[i].PrTimeEnd);
        }

        var source = $('#ProductListNotEdit-template').html();
        var template = Handlebars.compile(source);
        var html = template(data.rows);
        $('#ProductListSold').append(html);

        $('#ProductListSold tr[style]').fadeIn('slow', function(){
            $(this).removeAttr('style');
        });

        if(data.hasMore == 'false'){
            $('#LoadMoreProductListSold').hide();
        }else{
            $('#LoadMoreProductListSold').show();
        }

        pagePr++;

        $('#loaderSold').hide();

    }).fail(function(xhr, textStatus, error){
        console.log(xhr);
        console.log(textStatus);
        console.log(error);

        $('#ProductListSoldModal').modal('toggle');

        if (xhr.responseJSON.msg === 'verify fail') {
            $('#LoginModal').modal('show');
            swal("Thông Báo", "Token hết hạn, vui lòng đăng nhập lại.", "warning");
            $('#LoginModal').modal('show');
        } else if (xhr.responseJSON.msg === 'no token found') {
            $('#LoginModal').modal('show');
            swal("Thông Báo", "Không tìm thấy token, vui lòng đăng nhập.", "warning");
            $('#LoginModal').modal('show');
        } else {
            swal("Thông Báo", "Có lỗi xảy ra.", "warning");
        }
    });
}

// Load More Product List Not End
$('#LoadMoreProductListSold').click(function(){
    loadProductListSold();
});

// Click button Product List Sold View
$('#ProductListSold').on('click', '#btnProductListView', function(){
    var id = $(this).data('id');
    window.location.replace("http://localhost:8080/product?id=" + id);
});

// End Event of Show Product List Sold ///////////////////////////////////////////////

///---------------------------------------------------------------------------------------------------///

////////////////////////////////////////////// INDEX PAGE ///////////////////////////////////////////////////////

// Tìm kiếm sản phẩm

// Xem chuyên mục sản phẩm

// Load 5 sản phẩm có lượt ra giá nhiều nhất
$.ajax({
    url: 'http://localhost:6500/product/top5/nhieuluotragia',
    dataType: 'json',
    timeout: 10000
}).done(function(rows){

    for(var i = 0; i < rows.length; i++){
        val = rows[i];
        var source = $('#top5-template').html();
        var template = Handlebars.compile(source);
        var html = template(val);

        $('#Top5RaGia').append(html);
    }
}).fail(function(xhr, textStatus, error){
    console.log(xhr);
    console.log(textStatus);
    console.log(error);
});

// Load 5 sản phẩm có giá cao nhất
$.ajax({
    url: 'http://localhost:6500/product/top5/giacaonhat',
    dataType: 'json',
    timeout: 10000
}).done(function(rows){
    // Handler bar

    for(var i = 0; i < rows.length; i++){
        val = rows[i];
        var source = $('#top5-template').html();
        var template = Handlebars.compile(source);
        var html = template(val);

        $('#Top5CaoNhat').append(html);
    }
}).fail(function(xhr, textStatus, error){
    console.log(xhr);
    console.log(textStatus);
    console.log(error);
});

// Load 5 sản phẩm gần kết thúc
$.ajax({
    url: 'http://localhost:6500/product/top5/ganketthuc',
    dataType: 'json',
    timeout: 10000
}).done(function(rows){
    // Handler bar

    for(var i = 0; i < rows.length; i++){
        val = rows[i];
        var source = $('#top5-template').html();
        var template = Handlebars.compile(source);
        var html = template(val);

        $('#Top5KetThuc').append(html);
    }
}).fail(function(xhr, textStatus, error){
    console.log(xhr);
    console.log(textStatus);
    console.log(error);
});

$('#Top5RaGia').on('click', '#btnViewDetail', function(){
    var id = $(this).data('id');
    window.location.replace("http://localhost:8080/product?id=" + id);
});

$('#Top5CaoNhat').on('click', '#btnViewDetail', function(){
    var id = $(this).data('id');
    window.location.replace("http://localhost:8080/product?id=" + id);
});

$('#Top5KetThuc').on('click', '#btnViewDetail', function(){
    var id = $(this).data('id');
    window.location.replace("http://localhost:8080/product?id=" + id);
});


function insertWatchList(id){
    var poco = {
        WlUserID: localStorage.UserID,
        WlPrID: id
    }

    $.ajax({
        url: 'http://localhost:6500/watchlist',
        type: 'POST',
        dataType: 'json',
        contentType: 'application/json',
        timeout: 10000,
        data: JSON.stringify(poco),
        headers: {
            'x-access-token' : localStorage.access_token
        }
    }).done(function(data){
        console.log(data);
        swal("Thông báo!", "Thêm vào danh sách yêu thích thành công!", "success");
    }).fail(function(xhr, textStatus, err) {
        console.log(xhr);
        console.log(textStatus);
        console.log(err);

        if(xhr.responseJSON.name == "SequelizeUniqueConstraintError"){
            swal("Thông báo!", "Sản phẩm đã được thêm vào danh sách yêu thích!", "error");
        }else{
            swal("Thông báo!", "Có lỗi xảy ra!", "error");
        }
    });;
}

$('#Top5RaGia').on('click', '#btnAddWatchList', function(){
    var id = $(this).data('id');
    insertWatchList(id);
});

$('#Top5CaoNhat').on('click', '#btnAddWatchList', function(){
    var id = $(this).data('id');
    insertWatchList(id);
});

$('#Top5KetThuc').on('click', '#btnAddWatchList', function(){
    var id = $(this).data('id');
    insertWatchList(id);
});

////////////////////////////////////////////// END INDEX PAGE ///////////////////////////////////////////////////////

///====================================  SEARCH PAGE   ===============================================================///

// Event Search /////////////////////////////////////////////////////////////////////

$('#btnSearch').click(function(){
    var txtSearch = $('#txtSearch').val();
    var sltCatList = $('#CatListSearch').val();
    $('#ProductListSearch').empty();

    if(txtSearch == ''){
        swal("Thông báo!", "Nhập thông tin tìm kiếm!", "warning");
    }else{
        $('#SearchModal').modal('show');

        pageCat = 1;
        pageQuery = 1;
        search(txtSearch, sltCatList);
    }
});

$('#LoadMoreSearchWithCat').click(function(){
    var poco = {
        query: txtSearch,
        cat: sltCatList,
        page: pageCat
    }
    loadSearchWithCat(poco);
});

$('#LoadMoreSearchNotCat').click(function(){
    var poco = {
        query: txtSearch,
        page: pageQuery
    }
    loadSearchNotCat(poco);
});

function search(txtSearch, sltCatList){
    $('#LoadMoreSearchWithCat').hide();
    $('#LoadMoreSearchNoCat').hide();

    if(sltCatList > 0){
        var poco = {
            query: txtSearch,
            cat: sltCatList,
            page: pageCat
        }
        loadSearchWithCat(poco);
    }else if(txtSearch){
        var poco = {
            query: txtSearch,
            page: pageQuery
        }

        loadSearchNotCat(poco);
    }
}

function loadSearchWithCat(poco){
    $('#loaderSearch').show();
    $.ajax({
        url: 'http://localhost:6500/product/searchwithcat',
        dataType: 'json',
        type: 'POST',
        contentType: 'application/json',
        timeout: 10000,
        data: JSON.stringify(poco)
    }).done(function(data){
        $('#loaderSearch').show();
        var source = $('#Search-template').html();
        var template = Handlebars.compile(source);
        var html = template(data.rows);
        
        $('#ProductListSearch').append(html);

        pageCat++;
        if (data.hasMore == "false") {
            $('#LoadMoreSearchWithCat').hide();
        } else {
            $('#LoadMoreSearchWithCat').show();
        }

        $('#loaderSearch').hide();
    }).fail(function(xhr, textStatus, error){
        console.log(xhr);
        console.log(textStatus);
        console.log(error);
    });
}

function loadSearchNotCat(poco){
    $('#loaderSearch').show();
    $.ajax({
        url: 'http://localhost:6500/product/searchnotcat',
        dataType: 'json',
        type: 'POST',
        contentType: 'application/json',
        timeout: 10000,
        data: JSON.stringify(poco)
    }).done(function(data){
        $('#loaderSearch').show();
        var source = $('#Search-template').html();
        var template = Handlebars.compile(source);
        var html = template(data.rows);
        
        $('#ProductListSearch').append(html);

        pageQuery++;
        if (data.hasMore == "false") {
            $('#LoadMoreSearchNotCat').hide();
        } else {
            $('#LoadMoreSearchNotCat').show();
        }

        $('#loaderSearch').hide();
    }).fail(function(xhr, textStatus, error){
        console.log(xhr);
        console.log(textStatus);
        console.log(error);
    });
}