/*
Navicat MySQL Data Transfer

Source Server         : MySql Server
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : dack

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2018-06-20 14:23:10
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for bid
-- ----------------------------
DROP TABLE IF EXISTS `bid`;
CREATE TABLE `bid` (
  `BidID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `BidPrID` int(10) unsigned NOT NULL,
  `BidUserID` int(10) unsigned NOT NULL,
  `BidTimeBid` datetime DEFAULT NULL,
  `BidTotal` float DEFAULT NULL,
  `BidPriceMax` float DEFAULT NULL,
  PRIMARY KEY (`BidID`,`BidPrID`,`BidUserID`),
  KEY `FK_bid_user` (`BidUserID`),
  KEY `FK_bid_product` (`BidPrID`),
  CONSTRAINT `FK_bid_product` FOREIGN KEY (`BidPrID`) REFERENCES `product` (`PrID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_bid_user` FOREIGN KEY (`BidUserID`) REFERENCES `user` (`UserID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=102 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of bid
-- ----------------------------
INSERT INTO `bid` VALUES ('96', '11', '22', '2018-06-20 13:57:01', '10100000', null);
INSERT INTO `bid` VALUES ('97', '11', '21', '2018-06-20 13:57:33', '10200000', '10500000');
INSERT INTO `bid` VALUES ('98', '11', '21', '2018-06-20 13:57:50', '10500000', '10500000');
INSERT INTO `bid` VALUES ('100', '11', '24', '2018-06-20 14:10:36', '10600000', null);
INSERT INTO `bid` VALUES ('101', '11', '24', '2018-06-20 14:11:06', '10700000', null);

-- ----------------------------
-- Table structure for cat
-- ----------------------------
DROP TABLE IF EXISTS `cat`;
CREATE TABLE `cat` (
  `CatID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `CatName` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`CatID`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of cat
-- ----------------------------
INSERT INTO `cat` VALUES ('32', 'Điện gia dụng');
INSERT INTO `cat` VALUES ('33', 'Balo - giầy dép');
INSERT INTO `cat` VALUES ('34', 'Mẹ và bé');
INSERT INTO `cat` VALUES ('35', 'Điện máy');
INSERT INTO `cat` VALUES ('41', 'Điện Thoại');

-- ----------------------------
-- Table structure for img
-- ----------------------------
DROP TABLE IF EXISTS `img`;
CREATE TABLE `img` (
  `ImgID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ImgName` varchar(255) DEFAULT NULL,
  `ImgPrID` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`ImgID`),
  KEY `FK_img_product` (`ImgPrID`),
  CONSTRAINT `FK_img_product` FOREIGN KEY (`ImgPrID`) REFERENCES `product` (`PrID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of img
-- ----------------------------
INSERT INTO `img` VALUES ('2', '3_2254.jpeg', '2');
INSERT INTO `img` VALUES ('3', '3_2994.jpeg', '3');
INSERT INTO `img` VALUES ('4', '1_1193.jpeg', '11');
INSERT INTO `img` VALUES ('5', '2_2773.jpeg', '11');
INSERT INTO `img` VALUES ('6', '3_479.jpeg', '11');

-- ----------------------------
-- Table structure for kick
-- ----------------------------
DROP TABLE IF EXISTS `kick`;
CREATE TABLE `kick` (
  `KickPrID` int(10) unsigned NOT NULL,
  `KickUserID` int(10) unsigned NOT NULL,
  PRIMARY KEY (`KickPrID`,`KickUserID`),
  KEY `FK_kick_user` (`KickUserID`),
  CONSTRAINT `FK_kick_product` FOREIGN KEY (`KickPrID`) REFERENCES `product` (`PrID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_kick_user` FOREIGN KEY (`KickUserID`) REFERENCES `user` (`UserID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of kick
-- ----------------------------
INSERT INTO `kick` VALUES ('11', '24');

-- ----------------------------
-- Table structure for product
-- ----------------------------
DROP TABLE IF EXISTS `product`;
CREATE TABLE `product` (
  `PrID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `PrName` varchar(255) DEFAULT NULL,
  `PrContent` text,
  `PrCatID` int(10) unsigned DEFAULT NULL,
  `PrUserID` int(10) unsigned DEFAULT NULL,
  `PrAutoReup` int(2) DEFAULT NULL,
  `PrPriceOffer` float DEFAULT NULL,
  `PrPriceStep` float DEFAULT NULL,
  `PrPriceBuyNow` float DEFAULT NULL,
  `PrTimeStart` datetime DEFAULT NULL,
  `PrTimeEnd` datetime DEFAULT NULL,
  `PrDelayStatus` varchar(255) DEFAULT 'off',
  PRIMARY KEY (`PrID`),
  KEY `FK_product_cat` (`PrCatID`),
  KEY `FK_product_user` (`PrUserID`),
  CONSTRAINT `FK_product_cat` FOREIGN KEY (`PrCatID`) REFERENCES `cat` (`CatID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_product_user` FOREIGN KEY (`PrUserID`) REFERENCES `user` (`UserID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of product
-- ----------------------------
INSERT INTO `product` VALUES ('2', 'iphone X', '<p>abcd</p>\r\n', '34', '22', '0', '50000', '10000', null, '2018-06-16 07:34:04', '2018-06-16 22:35:00', 'off');
INSERT INTO `product` VALUES ('3', 'iphone X', '<p>abcd</p>\r\n', '34', '22', '0', '50000', '10000', null, '2018-06-16 07:34:00', '2018-06-16 07:55:00', 'off');
INSERT INTO `product` VALUES ('4', 'x', 'dfs', '34', '22', null, null, null, null, null, '2018-06-29 14:03:29', 'off');
INSERT INTO `product` VALUES ('5', 'y', 'dsfgsdfg', '34', '22', null, null, null, null, null, '2018-06-29 16:00:09', 'off');
INSERT INTO `product` VALUES ('6', 'v', 'dsfsdgs', '34', '22', null, null, null, null, null, '2018-06-30 11:00:16', 'off');
INSERT INTO `product` VALUES ('7', 'dfdg', 'sdfsdg', '34', '22', null, null, null, null, null, '2018-06-30 14:00:21', 'off');
INSERT INTO `product` VALUES ('8', 'fgsd', 'dfgsdg', '34', '22', null, null, null, null, null, '2018-06-15 14:00:24', 'off');
INSERT INTO `product` VALUES ('9', 'oppo 5', '', '34', '22', null, null, null, null, null, '2018-06-30 14:00:27', 'off');
INSERT INTO `product` VALUES ('10', 'samsung 6', '<p><strong>Update (17-06-2018) </strong></p>\n\n<p>them moi</p>\n<b>Update (17-06-2018) </b><p>them moi</p>\n', '34', '22', null, '50000', null, null, '2018-06-07 12:31:25', '2018-06-30 14:00:34', 'off');
INSERT INTO `product` VALUES ('11', 'tã giấy', '<p>tã gi&acirc;́y si&ecirc;u b&ecirc;̀n</p>\r\n', '34', '22', '0', '10000000', '100000', '15000000', '2018-06-18 17:25:00', '2018-06-20 17:26:00', 'off');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `UserID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `UserName` varchar(255) DEFAULT NULL,
  `UserEmail` varchar(255) DEFAULT NULL,
  `UserPwd` varchar(255) DEFAULT NULL,
  `UserAdd` tinytext,
  `UserLevel` int(2) unsigned DEFAULT '1',
  `UserType` varchar(255) DEFAULT 'buy',
  `UserTimePermission` datetime DEFAULT NULL,
  `UserTimeEndSeller` datetime DEFAULT NULL,
  PRIMARY KEY (`UserID`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('2', 'Admin', 'admin@gmail.com', '4297f44b13955235245b2497399d7a93', 'TP.HCM', '2', 'buy', null, null);
INSERT INTO `user` VALUES ('21', 'Long', 'long@gmail.com', '4297f44b13955235245b2497399d7a93', 'Long An', '1', 'buy', null, null);
INSERT INTO `user` VALUES ('22', 'Nam', 'nam@gmail.com', '4297f44b13955235245b2497399d7a93', 'Long An', '1', 'sell', null, '2018-05-22 19:33:44');
INSERT INTO `user` VALUES ('23', 'Nhat', 'nhat@gmail.com', '4297f44b13955235245b2497399d7a93', 'Lam Dong', '1', 'buy', null, null);
INSERT INTO `user` VALUES ('24', 'Minh', 'minh@gmail.com', '4297f44b13955235245b2497399d7a93', 'Nghe An', '1', 'buy', null, null);
INSERT INTO `user` VALUES ('25', 'Duong', 'duong@gmail.com', '4297f44b13955235245b2497399d7a93', 'Phu Quoc', '1', 'buy', null, null);
INSERT INTO `user` VALUES ('26', 'Uy', 'uy@gmail.com', '4297f44b13955235245b2497399d7a93', 'Hue', '1', 'buy', '2018-06-20 12:37:25', null);
INSERT INTO `user` VALUES ('27', 'Phat', 'phat@gmail.com', '4297f44b13955235245b2497399d7a93', 'Dak Lak', '1', 'buy', null, null);

-- ----------------------------
-- Table structure for watchlist
-- ----------------------------
DROP TABLE IF EXISTS `watchlist`;
CREATE TABLE `watchlist` (
  `WlUserID` int(10) unsigned NOT NULL,
  `WlPrID` int(10) unsigned NOT NULL,
  PRIMARY KEY (`WlUserID`,`WlPrID`),
  KEY `FK_wl_product` (`WlPrID`),
  CONSTRAINT `FK_wl_product` FOREIGN KEY (`WlPrID`) REFERENCES `product` (`PrID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_wl_user` FOREIGN KEY (`WlUserID`) REFERENCES `user` (`UserID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of watchlist
-- ----------------------------
INSERT INTO `watchlist` VALUES ('21', '4');
INSERT INTO `watchlist` VALUES ('21', '8');
